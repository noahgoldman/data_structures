# Code for Data Structures

This is all of the homeworks and labs that I wrote for Data Structures at RPI in Fall 2012.

# Warning

The cheating detection tool that they have in place for this class is very extensive and likely involves some form of analyzing the actual executable.  This means that changing simple things like variable names or indentation will not get around the plagiarism guard, and you would be unwise to try to use my code in any way on a homework.  Simply looking at it in order to understand high level concepts is probably the best way to use my work.
