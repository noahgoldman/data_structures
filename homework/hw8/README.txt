HOMEWORK 8: CITY CHASE


NAME: Noah Goldman 


TIME SPENT ON THIS ASSIGNMENT: 6 


COLLABORATORS AND OTHER RESOURCES:
Eric Culp

EVADER STRATEGY:
First, all of the possible locations that the evader can move to are sorted by how many pursuers can possibly move to it. The neighbors are then iterated through until one is found that doesn't have a pursuer already on it.  This node is the one that the evader will move to.


PURSUER STRATEGY:
I implemented dijksttra's algorithm to find the shortest path to a random neighbor node of the evader, but I then realized after that I couldn't get a vector of all the vertices with the graph interface that was provided.  I ended up using a global variable to accumulate all of the nodes through the various neighbors, then used that as the input for the algorithm.  While it might work well on small graphs, it probably will not work as well with large ones where it would take a long time to accumulate a full list of the nodes.


NEW GRAPH NETWORK FOR EXTRA CREDIT:
Describe your new network and why it makes a fun game.


MISC. COMMENTS TO GRADER:  
