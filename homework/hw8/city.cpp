#include <vector>
#include <string>
#include "city.h"

int City::addLink(City *city) {
  for (unsigned int i = 0; i < this->adjacents.size(); i++) {
    if (this->adjacents[i] == city) {
      return 0;
    }
  }
  this->adjacents.push_back(city);
  return 1;
}

int City::removeLink(const City *city) {
  for (std::vector<City*>::iterator itr = this->adjacents.begin();
      itr != this->adjacents.end(); itr++) {
    if (*itr == city) {
      this->adjacents.erase(itr);
      return 1;
    }
  }
  return 0;
}
