#include <iostream>
#include "city.h"
#include "person.h"

void Person::Move(City *city) {
  if (this->location == city) {
    std::cout << this->name << " stays in " << this->location->getName() <<
      std::endl;
  } else {
    std::cout << this->name << " moves from " << this->location->getName() <<
      " to " << city->getName() << std::endl;
    this->location = city;
  }
}
