#ifndef CITY_H_
#define CITY_H_

#include <string>
#include <vector>

class City {
  public:
    explicit City(const std::string &n) : name(n) {}

    inline const std::string& getName() const { return this->name; }

    int addLink(City *city);
    int removeLink(const City *city); 

    const std::vector<City*>& getNeighbors() const { return this->adjacents; }

  private:
    std::string name;
    std::vector<City*> adjacents;
};

#endif // CITY_H_
