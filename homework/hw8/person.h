#ifndef PERSON_H_
#define PERSON_H_

class City;

class Person {
  public:
    Person(const std::string& n, City *c) :  name(n), location(c) {}

    inline City* getLocation() const { return this->location; }
    inline const std::string& getName() const { return this->name; }

    void Move(City *city);

  private:
    std::string name;
    City* location;
};

#endif // PERSON_H_
