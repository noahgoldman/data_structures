#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <list>
#include <algorithm>
#include "MersenneTwister.h"
#include "person.h"
#include "city.h"
#include "graph.h"

std::vector<City*> cities;

// get all the cities that the graph algorithms interface will return and accumulate
// them in the global cities variable
void get_cities(const Graph& graph) {
  City *evader_city = graph.getEvader()->getLocation();
  const std::vector<City*>& evader_neighbors = evader_city->getNeighbors();
  cities.push_back(evader_city);
  for (unsigned int i = 0; i < evader_neighbors.size(); i++) {
    if (std::find(cities.begin(), cities.end(), evader_neighbors[i]) == cities.end()) {
      cities.push_back(evader_neighbors[i]);
    }
  }

  std::vector<Person*> pursuers = graph.getPursuers();
  for (unsigned int i = 0; i < pursuers.size(); i++) {
    City *pursuer_city = pursuers[i]->getLocation();
    const std::vector<City*> &neighbors = pursuer_city->getNeighbors();
    for (unsigned int j = 0; j < neighbors.size(); j++) {
      if (std::find(cities.begin(), cities.end(), evader_neighbors[i]) == cities.end()) {
        cities.push_back(neighbors[i]);
      }
    }
  }
}
     

// Implement dijkistra's algorithm
const std::list<City*>* find_shortest(const Person &p, const City* city, const Graph& graph) {
  get_cities(graph);
  std::map<City*, int> finished;
  std::map<City*, int> working;
  std::map<City*, City*> previous;
  std::list<City*> *path = new std::list<City*>;

  City *undefined = NULL;

  // initialize the distance to -1 (infinity)
  for (unsigned int i = 0; i < cities.size(); i++) {
    working.insert(std::make_pair(cities[i], -1));
    previous.insert(std::make_pair(cities[i], undefined));
  }

  finished[p.getLocation()] = 0;

  while (working.size() > 0) {
    City *node = working.begin()->first;
    int distance = working.begin()->second;
    
    // pop the city off of the working list
    working.erase(working.begin());

    if (node == city) {
      while (previous.find(node) != previous.end()) {
        path->push_front(node);
        node = previous[node];
      }
      return path;
    }

    // if the node is already finished, don't run the iteration
    if (finished.find(node) != finished.end()) continue;
     
    const std::vector<City*> neighbors = node->getNeighbors();
    for (unsigned int i = 0; i < neighbors.size(); i++) {
     int distance_between = distance + 1;
     std::map<City*, int>::iterator res = working.find(neighbors[i]);
     if (res != working.end() && distance_between < res->second) {
       working[neighbors[i]] = distance_between;
       previous[neighbors[i]] = node;
     }
   }
  }

  return path;
}

City* MY_PURSUER_STRATEGY(const Person& p, const Graph& city_graph) {
  // Pick a random neighbor of the evader to head to
  static MTRand rand;
  Person* evader = city_graph.getEvader();
  const std::vector<City*>& neighbors = evader->getLocation()->getNeighbors();
  int random = rand.randInt(neighbors.size() - 1);
  City *city = neighbors[random];

  const std::list<City*> *path = find_shortest(p, city, city_graph);
  City *next;
  if (path->size() > 0) {
    next = path->front();
  } else {
    next = p.getLocation();
  }
  delete path;
  return next;
}
