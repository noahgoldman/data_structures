#ifndef GRAPH_H_
#define GRAPH_H_

#include <vector>

class Person;
class City;

class Graph {
  public:
    Graph() : evader(NULL) {}
    ~Graph() { this->Delete(); }

    Person* getEvader() const { return this->evader; }
    const std::vector<Person*>& getPursuers() const { return this->pursuers; }

    int addCity(const std::string &name);
    int removeCity(const std::string &name);
    int addLink(const std::string &name1, const std::string &name2);
    int removeLink(const std::string &name1, const std::string &name2);
    int placeEvader(const std::string &pname, const std::string &cname);
    int placePursuer(const std::string &pname, const std::string &cname);
    bool tick(const std::string &evader_arg, const std::string &pursuer_arg);

    friend std::ostream& operator<<(std::ostream &ostr, Graph &city_graph);

  private:
    void Delete();

    void Print();
    int find(const std::string &name, City *&city);
    
    std::vector<City*> cities;
    Person* evader;
    std::vector<Person*> pursuers;
};

#endif // GRAPH_H_
