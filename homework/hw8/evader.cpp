#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>
#include "MersenneTwister.h"
#include "person.h"
#include "city.h"
#include "graph.h"

int num_pursuers_adjacent(const City *city, const Graph& city_graph) {
  const std::vector<Person*>& pursuers = city_graph.getPursuers();
  const std::vector<City*>& neighbors = city->getNeighbors();
  
  unsigned int count = 0;
  for (unsigned int i = 0; i < neighbors.size(); i++) {
    for (unsigned int j = 0; j < pursuers.size(); j++) {
      if (pursuers[j]->getLocation() == neighbors[i]) ++count;
    }
  }
  return count;
}

class sort_by_pursuers {
  public:
    sort_by_pursuers(const Graph& g) : graph(g) {}
    bool operator()(const City *c1, const City *c2) {
      return (num_pursuers_adjacent(c1, this->graph) < num_pursuers_adjacent(c2, this->graph));
    }
    const Graph& graph;
}; 


// Sort the possible places we can go by their amount of links
// don't go to places where the pursuers are
City* MY_EVADER_STRATEGY(const Person& p, const Graph& city_graph) {
  const std::vector<Person*>& pursuers = city_graph.getPursuers();

  std::vector<City*> neighbors = p.getLocation()->getNeighbors();
  std::sort(neighbors.begin(), neighbors.end(), sort_by_pursuers(city_graph));

  for (unsigned int i = 0; i < neighbors.size(); i++) {
    int found = 0;
    for (unsigned int j = 0; j < pursuers.size(); j++) {
      if (pursuers[j]->getLocation() == neighbors[i]) found = 1;
    }

    if (!found) {
      return neighbors[i];
    }
  }    

  return p.getLocation();
}
