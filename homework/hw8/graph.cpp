#include <assert.h>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include "city.h"
#include "graph.h"
#include "person.h"

int compare_cities(const City *c1, const City *c2) {
  return (c1->getName() < c2->getName());
}

void Graph::Delete() {
  // delete all the cities
  for (int i = (int) this->cities.size() - 1; i >= 0; i--) {
    delete this->cities[i];
    this->cities.pop_back();
  }

  // delete the evader
  if (this->evader != NULL) {
    delete this->evader;
  }

  // delete the pursuers
  for (int i = (int) this->pursuers.size() - 1; i >= 0; i--) {
    delete this->pursuers[i];
    this->pursuers.pop_back();
  }
}

int Graph::addCity(const std::string &name) {
  City *city = NULL;
  if (this->find(name, city)) {
    return 0;
  } else {
    city = new City(name);
    cities.push_back(city);
    return 1;
  }
}

int Graph::removeCity(const std::string &name) {
  // Find the city that we are deleting first and take it off
  // the cities list
  City *city = NULL;
  for (std::vector<City*>::iterator itr = this->cities.begin();
      itr != this->cities.end(); itr++) {
    if ((*itr)->getName() == name) {
      city = *itr;
      itr = this->cities.erase(itr);
      if (itr == this->cities.end()) --itr;
    }
  }

  // Delete any people that are in that city
  if (this->evader != NULL && this->evader->getLocation() == city) {
    std::cout << "Evader " << this->evader->getName() << " removed from the game" << std::endl;
    delete this->evader;
    this->evader = NULL;
  }
  for (std::vector<Person*>::iterator itr = this->pursuers.begin();
      itr != this->pursuers.end(); itr++) {
    if ((*itr)->getLocation() == city) {
      std::cout << "Pursuer " << (*itr)->getName() << " removed from the game" << std::endl;
      Person *persuer = *itr;
      itr = this->pursuers.erase(itr);
      delete persuer;
      if (itr == this->pursuers.end()) itr--;
    }
  }

  if (!city) {
    return 0;
  } else {
    for (unsigned int i = 0; i < this->cities.size(); i++) {
      this->cities[i]->removeLink(city);
    }

    delete city;  

    return 1;
  }
}

int Graph::addLink(const std::string &name1, const std::string &name2) {
  City *city1 = NULL;
  City *city2 = NULL;
  this->find(name1, city1);
  this->find(name2, city2);

  if (city1 == NULL || city2 == NULL) {
    return 0;
  }

  if (city1->addLink(city2) && city2->addLink(city1)) {
    return 1;
  } else {
    return 0;
  }
}

int Graph::removeLink(const std::string &name1, const std::string &name2) {
  City *city1 = NULL;
  City *city2 = NULL;
  this->find(name1, city1);
  this->find(name2, city2);

  if (city1 == NULL || city2 == NULL) {
    return 0;
  }

  if (city1->removeLink(city2) && city2->removeLink(city1)) {
    return 1;
  } else {
    return 0;
  }
}

int Graph::placeEvader(const std::string &pname, const std::string &cname) {
  City *city = NULL;
  this->find(cname, city);

  if (city != NULL) {
    if (this->evader != NULL) {
      delete this->evader;
    }

    this->evader = new Person(pname, city);
    return 1;
  }
  return 0;
}

int Graph::placePursuer(const std::string &pname, const std::string &cname) {
  City *city = NULL;
  this->find(cname, city);

  if (city != NULL) {
    for (unsigned int i = 0; i < this->pursuers.size(); i++) {
      if (this->pursuers[i]->getName() == pname) {
        return 0;
      }
    }
    Person *pursuer = new Person(pname, city);
    this->pursuers.push_back(pursuer);
    return 1;
  } else {
   return 0;
  } 
}

std::ostream& operator<<(std::ostream &ostr, Graph &graph) {
  std::sort(graph.cities.begin(), graph.cities.end(), compare_cities);

  ostr << std::endl;
  for (unsigned int i = 0; i < graph.cities.size(); i++) {
    std::vector<City*> adjacents = graph.cities[i]->getNeighbors(); 
    std::sort(adjacents.begin(), adjacents.end(), compare_cities);
    ostr << "Neighbors for city " << graph.cities[i]->getName() << ": ";

    if (adjacents.size() > 0) {
      for (unsigned int i = 0; i < adjacents.size(); i++) {
        ostr << " " << adjacents[i]->getName();
      }
    }
    ostr << std::endl;
  }

  if (graph.evader != NULL) {
    ostr << "Evader " << graph.evader->getName() << " is in " << 
      graph.evader->getLocation()->getName() << std::endl;
  }
  for (unsigned int i = 0; i < graph.pursuers.size(); i++) {
    ostr << "Pursuer " << graph.pursuers[i]->getName() << " is in " <<
      graph.pursuers[i]->getLocation()->getName() << std::endl;
  }
  ostr << std::endl;
  return ostr;
}


int Graph::find(const std::string &name, City *&city) {
  for (unsigned int i = 0; i < this->cities.size(); i++) {
    if (this->cities[i]->getName() == name) {
      city = this->cities[i];
      return 1;
    }
  }
  return 0;
}
