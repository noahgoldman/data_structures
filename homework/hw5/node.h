#ifndef NODE_H_
#define NODE_H_

#include "stdlib.h"

template <class T> class Node {
  public:
    Node() { this->Init(); }
    explicit Node(const T& v);
    T value;

    Node* chrono_next;
    Node* chrono_prev;

    Node* sorted_next;
    Node* sorted_prev;

    Node* random_next;

  private:
    void Init();
};

template <class T> void Node<T>::Init() {
  this->chrono_next = NULL;
  this->chrono_prev = NULL;
  this->sorted_next = NULL;
  this->sorted_prev = NULL;
  this->random_next = NULL;
}

template <class T> Node<T>::Node(const T& v) {
  this->Init();
  this->value = v;
}

#endif  // NODE_H_
