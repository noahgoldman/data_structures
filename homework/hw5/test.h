#ifndef _TEST_H_
#define _TEST_H_

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { const char *message = test(); tests_run++; \
                                 if (message) return message; } while (0)

#endif
