HOMEWORK 5: MULTI LINKED LISTS


NAME: Noah Goldman


TIME SPEND ON THIS ASSIGNMENT: 8



COLLABORATORS: 
You must do this assignment on your own, as described in the CS2
Academic Integrity Policy.  If you did discuss the problem or error
messages, etc. with anyone, please list their names here.



TESTING & DEBUGGING STRATEGY:
Trying to test every corner case I could think of.


ORDER NOTATION of the member functions of MultiLL?
(in terms of n, the number of elements currently in the list)

default constructor: O(1)

copy constructor: O(n)

assignment operator: O(n)

destructor: O(n)

size: O(1)

empty: O(1)

clear: O(n)

add: O(n)

erase: O(1)

begin_chronological: O(1)

end_chronological: O(1)

begin_sorted: O(1)

end_sorted: O(1)

begin_random: O(n^2)


ITERATOR INVALIDATION:
In what cases will iterators of the MultiLL be invalidated?  Justify
the need for each of the restrictions placed on your implementation.




EXTRA CREDIT: 
Which function in your implementation can be improved by a
non-insertion sort sorting method?  Did you implement the improvement?
Describe.




MISC. COMMENTS TO GRADER:  
Optional, please be concise!


