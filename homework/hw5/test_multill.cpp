#include <stdio.h>
#include <iostream>
#include "node.h"
#include "multi_linked_list.h"
#include "test.h"

int tests_run = 0;

static const char* test_node() {
  Node<int> *n = new Node<int>(23);
  mu_assert("Node constructor value fails", n->value == 23);
  mu_assert("Node init fails", n->sorted_prev == NULL);
  delete n;
  return 0;
}

static const char* test_multill_constructor() {
  MultiLL<int> *m = new MultiLL<int>;
  mu_assert("Size from constructor fails", m->size() == 0);
  delete m;
  return 0;
}

static const char* test_add() {
  MultiLL<int> *m = new MultiLL<int>;
  m->add(10);
  m->add(5);
  m->add(10);
  m->add(2);
  m->add(10);
  m->add(203);
  m->add(324);
  m->add(124);
  m->add(98);
  m->add(67);
  m->add(10);
  m->add(5);
  m->add(10);
  m->add(2);
  m->add(10);
  //mu_assert("Count after insertion is incorrect", m->size() == 5);

  MultiLL<int> *m2 = new MultiLL<int>(*m);
  for (MultiLL<int>::iterator it = m2->begin_chronological();
      it != m2->end_chronological(); it++) {
    std::cout << *it << std::endl;
  }
  std::cout << std::endl;
  std::cout << std::endl;
  m->clear();
  m2->clear();
  delete m;
  delete m2;
  return 0;
}

static const char* test_delete() {
  MultiLL<int> *m = new MultiLL<int>;
  m->add(10);
  //mu_assert("Count after insertion is incorrect", m->size() == 11);


  m->clear();
  /*for (MultiLL<int>::iterator it = m->begin_random();
      it != m->end_chronological(); it++) {
    std::cout << *it << std::endl;
  }
  */
  delete m;
  return 0;
}

static const char* all_tests() {
  mu_run_test(test_node);
  mu_run_test(test_multill_constructor);
  mu_run_test(test_add);
  mu_run_test(test_delete);
  return 0;
}

int main() {
  const char *result = all_tests();
  if (result != 0) {
    printf("%s\n", result);
  }
  else {
    printf("ALL TESTS PASSED\n");
  }
  printf("Tests run: %d\n", tests_run);

  return result != 0;
}
