#ifndef MULTI_LINKED_LIST_H_
#define MULTI_LINKED_LIST_H_

#include <assert.h>
#include "node.h"
#include "debug.h"
#include "mersenne_twister.h"

enum Type {chrono, rando, sorted};

template <class T> class MultiLL;

template <class T> class list_iterator {
  public:
    explicit list_iterator(const Type t) : ptr(NULL) { this->Init(t); }
    list_iterator(Node<T> *p, Type t) : ptr(p), type(t) {}
    list_iterator(const list_iterator<T> &old) : ptr(old.ptr), type(old.type) {}
    ~list_iterator() {}

    list_iterator<T>& operator=(const list_iterator<T> &old);
    T& operator*() { return this->ptr->value; }
    list_iterator<T>& operator++();
    list_iterator<T> operator++(int i);
    list_iterator<T>& operator--();
    list_iterator<T> operator--(int i);

    friend class MultiLL<T>;

    friend bool operator==(const list_iterator<T> &l, const list_iterator<T> &r) {
      return l.ptr == r.ptr; }
    friend bool operator!=(const list_iterator<T> &l, const list_iterator<T> &r) {
      return l.ptr != r.ptr; }

  private:
    void Init(const Type t);

    void next();
    void prev();

    Node<T> *ptr;
    Type type;
};

template <class T> list_iterator<T>& list_iterator<T>::operator=(
    const list_iterator<T> &old) {
  this->ptr = old.ptr;
  this->type = old.type;
  return *this;
}

template <class T> list_iterator<T>& list_iterator<T>::operator++() {
  this->next();
  return *this;
}

template <class T> list_iterator<T> list_iterator<T>::operator++(int i) {
  list_iterator<T> temp(*this);
  this->next();
  return temp;
}

template <class T> list_iterator<T>& list_iterator<T>::operator--() {
  this->prev();
  return *this;
}

template <class T> list_iterator<T> list_iterator<T>::operator--(int i) {
  list_iterator<T> temp(*this);
  this->prev();
  return temp;
}

template <class T> void list_iterator<T>::next() {
  switch (this->type) {
    case chrono:
      this->ptr = this->ptr->chrono_next;
      break;
    case rando:
      this->ptr = this->ptr->random_next;
      break;
    case sorted:
      this->ptr = this->ptr->sorted_next;
      break;
  }
}

template <class T> void list_iterator<T>::prev() {
  switch (this->type) {
    case chrono:
      this->ptr = this->ptr->chrono_prev;
      break;
    case rando:
      warn("Can't go back with a random iterator");
      break;
    case sorted:
      this->ptr = this->ptr->sorted_prev;
      break;
  }
}

template <class T> class MultiLL {
  public:
    MultiLL() { this->Init(); }
    MultiLL(const MultiLL &ml) { this->Clone(ml); }
    MultiLL& operator=(const MultiLL &ml);
    ~MultiLL() { if (!this->empty())
      {this->ClearAll(this->chrono_head); this->Init(); }}

    void add(const T& v);

    int size() const { return this->count; }
    int empty() const { return this->chrono_head == NULL; }
    void clear() { if (!this->empty()) {this->ClearAll(this->chrono_head); this->Init(); }}

    typedef list_iterator<T> iterator;
    iterator begin_chronological() { return iterator(chrono_head, chrono); }
    iterator end_chronological() { return iterator(NULL, chrono); }
    iterator begin_sorted() { return iterator(sorted_head, sorted); }
    iterator end_sorted() { return iterator(NULL, sorted); }
    iterator begin_random();
    
    iterator erase(iterator itr);

  private:
    void Init();
    void Clear();
    void ClearAll(Node<T>*& head);
    void Clone(const MultiLL<T> &ml);
    void Randomize();

    int check_in_randomed(iterator i);

    void add_chrono(Node<T> *node);
    void add_sorted(Node<T> *v);

    Node<T> *chrono_head;
    Node<T> *chrono_tail;

    Node<T> *sorted_head;
    Node<T> *sorted_tail;

    Node<T> *random_head;

    int count;
};

template <class T> void MultiLL<T>::Init() {
  this->chrono_head = NULL;
  this->chrono_tail = NULL;
  this->sorted_head = NULL;
  this->sorted_tail = NULL;
  this->random_head = NULL;
  this->count = 0;
}

template <class T> void MultiLL<T>::Clone(const MultiLL<T> &ml) {
  this->Init();
  Node<T>* head = ml.chrono_head;
  while (head != NULL) {
    this->add(head->value);
    head = head->chrono_next;
  }
}

template <class T> MultiLL<T>& MultiLL<T>::operator=(const MultiLL<T> &ml) {
  if (this != &ml) {
    this->ClearAll(this->chrono_head);
    this->Clone(ml);
  }
  return *this;
}

template <class T> void MultiLL<T>::add(const T& v) {
  Node<T> *node = new Node<T>(v);
  // Create all the pointers to the element if the list is empty
  if (this->chrono_head == NULL && this->chrono_tail == NULL) {
    this->chrono_head = this->chrono_tail = this->sorted_head =
      this->sorted_tail = node;
  } else {
    this->add_chrono(node);
    this->add_sorted(node);
  }
  count++;
}  

template <class T> void MultiLL<T>::add_chrono(Node<T> *node) {
  this->chrono_tail->chrono_next = node;
  node->chrono_prev = this->chrono_tail;
  this->chrono_tail = node;
}

template <class T> void MultiLL<T>::add_sorted(Node<T> *node) {
  iterator i = this->begin_sorted();
  while (i != this->end_sorted() && node->value > *i) {
    i++;
  }
  
  if (i == this->end_sorted()) {
    this->sorted_tail->sorted_next = node;
    node->sorted_prev = this->sorted_tail;
    this->sorted_tail = node;
  } else {
    Node<T> *insert_node = i.ptr;
    if (insert_node == this->sorted_head) {
      this->sorted_head = node;
    } else {
      insert_node->sorted_prev->sorted_next = node;
    }
    node->sorted_prev = insert_node->sorted_prev;
    insert_node->sorted_prev = node;

    node->sorted_next = insert_node;
  }
}

template <class T> typename MultiLL<T>::iterator MultiLL<T>::erase(iterator itr) {
  assert(count > 0);
  // Create the iterator to the next element
  iterator res = itr;
  iterator check = itr;
  ++check;
  if (check.ptr == NULL) {
    res.ptr = NULL;
  } else {
    res++;
  }

  Node<T> *node = itr.ptr;
  // If its the chrono tail
  if (node->chrono_next == NULL) {
    this->chrono_tail = node->chrono_prev;
  } else {
    node->chrono_next->chrono_prev = node->chrono_prev;
  }
  // If its the sorted tail
  if (node->sorted_next == NULL) {
    this->sorted_tail = node->sorted_prev;
  } else {
    node->sorted_next->sorted_prev = node->sorted_prev;
  }

  // If its the chrono head
  if (node->chrono_prev == NULL) {
    this->chrono_head = node->chrono_next;
  } else {
    node->chrono_prev->chrono_next = node->chrono_next;
  }

  // If its the sorted head
  if (node->sorted_prev == NULL) {
    this->sorted_head = node->sorted_next;
  } else {
    node->sorted_prev->sorted_next = node->sorted_next;
  }

  delete node;
  count--;
  if (itr.type == rando) this->Randomize();
  return res;
}

template <class T> void MultiLL<T>::Clear() {
  Node<T> *current = this->chrono_head;
  while (current != NULL) {
    Node<T> *temp = current->chrono_next;
    delete current;
    current = NULL;
    current = temp;
  }
  this->Init();
}

template <class T> void MultiLL<T>::ClearAll(Node<T>*& head) {
  if (head != NULL) {
    Node<T> *tmp = head->chrono_next;
    delete head;
    head = NULL;
    this->ClearAll(tmp);
  } else {
    return;
  }
}

template <class T> typename MultiLL<T>::iterator MultiLL<T>::begin_random() {
  this->random_head = NULL;
  this->Randomize();
  return iterator(this->random_head, rando);
}

template <class T> void MultiLL<T>::Randomize() {
  if (this->count > 0) {
    // Iterate through all elements and delete their random pointers
    for (iterator itr = this->begin_chronological(); itr != this->end_chronological(); itr++) {
      itr.ptr->random_next = NULL;
    }

    MTRand gen;
    Node<T> *last_node = NULL;
    for (int r = 0; r < this->count; r++) {
      iterator element = this->begin_chronological();
      int k = gen.randInt(this->count - 1 - r);
      // Get to the kth element
      for (int i = 0; i < k && element != this->end_chronological(); i++) {
        element++;
      } 
      while (r > 0 && this->check_in_randomed(element) && element != this->end_chronological()) {
        element++;
      }
      Node<T> *node = element.ptr;
      if (last_node == NULL) {
        this->random_head = node;
      } else {
        last_node->random_next = node;
      }
      last_node = node;
    }
    last_node->random_next = this->random_head;
  }
}

template <class T> int MultiLL<T>::check_in_randomed(iterator i) {
  for (iterator itr(this->random_head, rando); itr != this->end_chronological(); itr++) {
    if (itr == i) {
      return 1;
    }
  }
  return 0;
}

#endif  // MULTI_LINKED_LIST_H_
