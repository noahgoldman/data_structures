HOMEWORK 1: MOIRE STRINGS


NAME: Noah Goldman 


COLLABORATORS: 
You must do this assignment on your own, as described in the CS2
Academic Integrity Policy.  If you did discuss the problem or errors
messages, etc. with anyone, please list their names here.



ESTIMATE OF # OF HOURS SPENT ON THIS ASSIGNMENT:
6


ANSWERS TO PART 1:

1.
The function includes a for loop that runs an amount of iterations equal to n. Therefore, the algorithm has an oder notation of O(n).


2.
This function includes two for loops, one of which is nested in the other.  Even though the constant part of the order notation might be smaller due to the fact that the inner loop depends on the value of the outers index, it still simplifies to n^2.  This shows that the order notation is O(n^2).

This function can be simplified as:

void subsequence_sum(float arr[], int n, float sums[]) {
  for (int i = 0; i < n; i++) {
    sums[n-1] += arr[i];
  }
  for (int j = n - 2; j >= 0; j++) {
    sums[j] = sums[j+1] - arr[j+1];
  }
}

This algorithm loops through n times to find the max sum, then loops another n times to complete the cumulative sum.  This would cause the order notation to be 2n, which can be simplified to O(n).  This is significantly better than the original implementation.


3.
Since the numbers of recursive function calls needed is exactly n, the order notation for the algorithm is O(n).


4.
The outer loop with run sqrt(n) times and the inner loop with run n times.  Multiplying these together gives n^1.5, showing that the order notation should be O(n^1.5) or O(n(sqrt(n))).



PART 2, EXTRA CREDIT SHAPES:
Please be concise!


Trapezoid:

    A trapezoid can be created with:
    ./patterns "asdf" 10 trapezoid out.txt
    
    Producing an output of:

             ***********
            *asdfasdfasd*
           *fasdfasdfasdf*
          *asdfasdfasdfasd*
         *fasdfasdfasdfasdf*
        *asdfasdfasdfasdfasd*
       *fasdfasdfasdfasdfasdf*
      *asdfasdfasdfasdfasdfasd*
     *fasdfasdfasdfasdfasdfasdf*
    *****************************

    They can also be created with:
    ./patterns "|_|" 5 trapezoid out.txt

    Producing an output of:

        ***********
       *|_|_|_|_|_|*
      *_|_|_|_|_|_|_*
     *|_|_|_|_|_|_|_|*
    *******************
 
Parallelogram:

    A parallelogram can be created with:
    ./patterns "asdf" 10 parallelogram out.txt

    Producing an output of:

    **********
     *asdfasdf*
      *asdfasdf*
       *asdfasdf*
        *asdfasdf*
         *asdfasdf*
          *asdfasdf*
           *asdfasdf*
            *asdfasdf*
             **********     

    They can also be created with:
    ./patterns "|_|" 5 parallelogram out.txt

    Producing an output of:
    
    *****
     *|_|*
      *|_|*
       *|_|*
        *****


MISC. COMMENTS TO GRADER:  
Optional, please be concise!

Quotes are included in the examples because my linux shell has problems 
with special characters such as "|".  However, the quotes are not necessary
for normal strings and might not be needed on other platforms.
