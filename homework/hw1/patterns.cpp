#include <iostream>
#include <fstream>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// This function throws an error message an exit
void die(const char *error) {
  std::cerr << "[ERROR]" << error << std::endl;
  exit(1);
}

// Return the next character from the original string that should be printed
// First check if the array index is within bounds, if not then reset it to the 
// beginning of the string
char next_letter(char string[], int data_length, int &index) {
  char output = string[index];
  index++;
  if (!(index < data_length)) index = 0;

  assert(index < data_length);

  return output;
}

// The function returns the minimun and maximum distance from the left of the 
// file at which characters should be printed.  It modified the variables 
// min and max by reference, altering them based on the current index of the
// loop
void set_limits(std::string shape, int index, int height, int &min, int &max) {
  // For a square, min and max are constant.
  if (shape == "square") {
    min = 0;
    max = height;
  }
  // The maximum character spot increases along with the loop index
  else if (shape == "right_triangle") {
    min = 0;
    max = index + 1;
  }
  // The isoceles triable varies both min and max as index increases
  else if (shape == "isosceles_triangle") {
    int width = (2*height - 1);   
    // The inherent trucation of int should come in handy here
    int center = width / 2;

    min = center - index;
    max = center + index + 1;
  }
  // The trapezoid is effectively just an isoceles triangle a constant
  // added to min, max, and the width
  else if (shape == "trapezoid") {
    int starting_width = 5;
    int width = (2*height + 3) + starting_width;

    int center = width / 2;

    min = center - index - starting_width;
    max = center + index + 1 + starting_width;
  }
  // A parallelogram is simply a trazpezoid with two parallel sides
  else if (shape == "parallelogram") {
    // The width of the parallelogram should be the total height of the shape
    int base_width = height;
    int width = 2*height;

    min = index;
    max = index + base_width;
  }
  /*
  else if (shape == "hexagon") {
    // A hexagon is only possible with heights of 5 or more
    if (height < 5) die("A hexagon must have at least 5 sides");

    int base_width = height;
    int width = base_width + height;
    int center = width / 2;
    if (index < (height / 2)) {
      min = center - index;
      max = center + index + base_width;
    }
    else if (index > (height / 2)) {
      min = center - (height - index);
      max = center + (height - index);
    }
  }
  */
  else {
    die("The inputted shape was not recognized");
  }
}

// This function converts command line inputs as strings into ints. Using
// strtol() is it possibly to verify that a true int was passes, unlike atoi()
int str_to_int(char *str) {
  char *endptr;
  int output = strtol(str, &endptr, 10);
  if (*endptr == '\0' && *str != '\0') {
    if (output < 0) die("The shape cannot have a negative height");
    return output;
  }
  else {
    die("The second argument must be an integer");
  }
}

int main(int argc, char *argv[]) {
  // Verify the command line arguments and throw errors if there are problems.
  if (argc != 5) die("Invalid number of arguments specified");

  std::string shape = argv[3];
  int height = str_to_int(argv[2]);

  std::ofstream file;
  file.open(argv[4]);

  if (!file.is_open()) die("The output file stream failed to open");


  // Define loop variables
  int min, max;
  int string_index = 0;
  int i, j;
  for (i = 0; i < height; i++) {
    // For every row of the file, get new limits
    set_limits(shape, i, height, min, max);
    for (j = 0; j < max; j++) {
      // If we havent reached the minimum limit yet, print whitespace
      if (j < min) {
        file << " ";
      }
      // At the edges of the shape, print a "*"
      else if (i == 0 || i == height - 1 || j == min || j == max - 1) file << "*";
      // Inside the actual shape, the letters are printed
      else if (j > min && j < max) {
        file << next_letter(argv[1], strlen(argv[1]), string_index);
      }

      // If we are at the end of the line, start a new one
      if (j == max - 1) file << "\n";
    }
  }

  // Close the file at the end of the program
  file.close();

  return 0;
}
