#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include <string>
#include <vector>
#include "utilities.h"
#include "polygons.h"

class Triangle : public Polygon {
  public:
    Triangle(const std::string &name, const std::vector<Point> &points);

  protected:
    int checkIsosceles() const;
};

class IsoscelesTriangle : virtual public Triangle {
  public:
    IsoscelesTriangle(const std::string &name, const std::vector<Point> &points);

  protected:
    int checkIsosceles() const;
};

class EquilateralTriangle : public IsoscelesTriangle {
  public:
    EquilateralTriangle(const std::string &name, const std::vector<Point> &points);
};

class RightTriangle : virtual public Triangle {
  public:
    RightTriangle(const std::string &name, const std::vector<Point> &points);

  protected:
    int checkRight() const;
};

class IsoscelesRightTriangle : public RightTriangle, public IsoscelesTriangle {
  public: 
    IsoscelesRightTriangle(const std::string &name, const std::vector<Point> &points);
};
#endif // TRIANGLE_H_
