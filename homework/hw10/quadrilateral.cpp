#include "stdio.h"
#include "quadrilateral.h"

Quadrilateral::Quadrilateral(const std::string &name, const std::vector<Point> &points)
    : Polygon(name, points) {
  if (this->points.size() != 4) {
    throw 1;
  }
}

Trapezoid::Trapezoid(const std::string &name, const std::vector<Point> &points)
    : Quadrilateral(name, points) {
  int good = 0;
  std::vector<double> angles;
  this->getAllAngles(angles);
  angles.push_back(angles[0]);

  for (unsigned int i = 0; i < angles.size() - 1; i++) {
    double added = angles[i] + angles[i+1];
    if (EqualAngles(added, 180)) {
      good = 1;
    }
  }
  if (good == 0) {
    throw 1;
  }
} 

int Trapezoid::countParallel() const {
  std::vector<double> angles;
  this->getAllAngles(angles);

  unsigned int count = 0;
  for (unsigned int i = 0; i < 2; i++) {
    if (EqualAngles(angles[i], angles[i+2])) ++count;
  }
  return count;
}

int Trapezoid::countEqualPairs() const {
  std::vector<Vector> vectors;
  this->getAllVectors(vectors);

  unsigned int count = 0;
  for (unsigned int i = 0; i < vectors.size(); i++) {
    for (unsigned int j = i + 1; j < vectors.size(); j++) {
      if (EqualSides(vectors[i].Length(), vectors[j].Length())) ++count;
    }
  }
  return count;
}

Parallelogram::Parallelogram(const std::string &name, const std::vector<Point> &points)
    : Quadrilateral(name, points), Trapezoid(name, points) {
  std::vector<double> angles;
  this->getAllAngles(angles);
  angles.push_back(angles[0]); 

  int good = 1;
  for (unsigned int i = 0; i < angles.size() - 1; i++) {
    double added = angles[i] + angles[i+1];
    if (!EqualAngles(added, 180)) {
      good = 0;
    }
  }
  
  if (!good) {
    throw 1;
  }
}

IsoscelesTrapezoid::IsoscelesTrapezoid(const std::string &name, const std::vector<Point> &points)
    : Quadrilateral(name, points), Trapezoid(name, points) {
  std::vector<double> angles;
  this->getAllAngles(angles);
  angles.push_back(angles[0]); 

  int good = 1;
  for (unsigned int i = 0; i < angles.size() - 2; i++) {
    double added = angles[i] + angles[i+2];
    if (!EqualAngles(added, 180)) {
      good = 0;
    }
  }

  if (!good) {
    throw 1;
  }
}

Rectangle::Rectangle(const std::string &name, const std::vector<Point> &points)
    : Quadrilateral(name, points), Trapezoid(name, points), Parallelogram(name, points),
      IsoscelesTrapezoid(name, points) {
  if (!this->HasAllEqualAngles()) {
    throw 1;
  }
}

Square::Square(const std::string &name, const std::vector<Point> &points)
    : Quadrilateral(name, points), Trapezoid(name, points), Parallelogram(name, points),
      IsoscelesTrapezoid(name, points), Kite(name, points), Rectangle(name, points),
      Rhombus(name, points) {
  if (!this->HasAllEqualSides() || !this->HasAllEqualAngles()) {
    throw 1;
  }
}

Rhombus::Rhombus(const std::string &name, const std::vector<Point> &points)
    : Quadrilateral(name, points), Trapezoid(name, points), Parallelogram(name, points),
      Kite(name, points) {
  if (!this->HasAllEqualSides()) {
    throw 1;
  }
}

int Kite::checkKite() const {
  std::vector<double> sides;
  this->getAllSides(sides);

  if (EqualSides(sides[0], sides[1]) && EqualSides(sides[2], sides[3])) return 1;
  if (EqualSides(sides[1], sides[2]) && EqualSides(sides[3], sides[0])) return 1;

  return 0;
}

Kite::Kite(const std::string &name, const std::vector<Point> &points)
    : Quadrilateral(name, points) {
  if (!this->checkKite()) {
    throw 1;
  }
}
