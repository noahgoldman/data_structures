#ifndef SHAPE_H_
#define SHAPE_H_

#include <vector>
#include <string>
#include "utilities.h"

class Polygon {
  public:
    Polygon(const std::string &name, const std::vector<Point> &points);
    virtual ~Polygon() { }

    const std::string& getName() const { return name; }

    int HasAllEqualSides() const;
    int HasAllEqualAngles() const;
    int HasARightAngle() const;

  protected:
    int countEqualPairs() const;
    void getAllVectors(std::vector<Vector> &vectors) const;
    void getCircularPoints(std::vector<Point> &cpoints) const;
    void getAllAngles(std::vector<double> &angles) const;
    void getAllSides(std::vector<double> &sides) const;

    const std::string name;
    const std::vector<Point> points;
};

#endif // SHAPE_H_
