#include "polygons.h"
#include "utilities.h"

Polygon::Polygon(const std::string &name, const std::vector<Point> &points)
    : name(name),
    points(points) {}

void Polygon::getAllVectors(std::vector<Vector> &vectors) const {
  for (unsigned int i = 0; i < this->points.size() - 1; i++) {
    Vector vect(this->points[i], this->points[i+1]);
    vect.Normalize();
    vectors.push_back(vect);
  }
}

void Polygon::getCircularPoints(std::vector<Point> &cpoints) const {
  for (unsigned int i = 0; i < this->points.size(); i++) {
    cpoints.push_back(this->points[i]);
  }
  for (unsigned int i = 0; i < this->points.size(); i++) {
    cpoints.push_back(this->points[i]);
  }
}

void Polygon::getAllAngles(std::vector<double> &angles) const {
  std::vector<Point> cpoints;
  this->getCircularPoints(cpoints);

  for (unsigned int i = 1; i <= this->points.size(); i++) {
    double angle = Angle(cpoints[i-1], cpoints[i], cpoints[i+1]);
    angles.push_back(angle);
  }
}

void Polygon::getAllSides(std::vector<double> &sides) const {
  std::vector<Point> cpoints;
  this->getCircularPoints(cpoints);

  for (unsigned int i = 0; i < this->points.size(); i++) {
    double dist = DistanceBetween(cpoints[i], cpoints[i+1]);
    sides.push_back(dist);
  }
}

int Polygon::HasAllEqualSides() const {
  std::vector<Point> cpoints;
  this->getCircularPoints(cpoints);

  double side = 0, last = 0;
  for (unsigned int i = 0; i < this->points.size() - 1; i++) {
    if (!last) {
      last = DistanceBetween(cpoints[i], cpoints[i + 1]);
    } else {
      side = DistanceBetween(cpoints[i], cpoints[i + 1]);
      if (!EqualSides(side, last)) {
        return 0;
      }
      last = side;
    }
  }
  return 1;
}

int Polygon::HasAllEqualAngles() const {
  std::vector<Point> cpoints;
  this->getCircularPoints(cpoints);

  double angle = 0, last = 0;
  for (unsigned int i = 1; i <= this->points.size(); i++) {
    if (!last) {
      last = Angle(cpoints[i-1], cpoints[i], cpoints[i+1]);
    } else {
      angle = Angle(cpoints[i-1], cpoints[i], cpoints[i+1]);
      if (!EqualAngles(angle, last)) {
        return 0;
      }
      last = angle;
    }
  }
  return 1;
}

int Polygon::countEqualPairs() const {
  std::vector<double> distances;
  unsigned int count = 0;
  for (unsigned int i = 0; i < this->points.size(); i++) {
    for (unsigned int j = i + 1; j <= this->points.size(); j++) {
      double side = DistanceBetween(points[i], points[j]);
      int found = 0;
      for (unsigned int k = 0; k < distances.size(); k++) {
        if (EqualSides(distances[k], side)) {
          found = 1;
        }
      }
      if (found) {
        ++count;
      } else {
        distances.push_back(side);
      }
    }
  }
  return count;
}

int Polygon::HasARightAngle() const {
  std::vector<Point> cpoints;
  this->getCircularPoints(cpoints);

  for (unsigned int i = 1; i <= this->points.size(); i++) {
    double angle = Angle(cpoints[i-1], cpoints[i], cpoints[i+1]);
    if (RightAngle(angle)) {
      return 1;
    }
  }
  return 0;
}
