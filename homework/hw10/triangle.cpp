#include <cmath>
#include "triangle.h"
#include "utilities.h"
#include "polygons.h"

Triangle::Triangle(const std::string &name, const std::vector<Point> &points)
    : Polygon(name, points) {
  if (points.size() != 3) {
    throw 1;
  }
}

int IsoscelesTriangle::checkIsosceles() const {
  std::vector<Point> cpoints;
  this->getCircularPoints(cpoints);
  std::vector<double> sides;
  for (unsigned int i = 0; i < this->points.size(); i++) {
    double dist = DistanceBetween(cpoints[i], cpoints[i+1]);
    sides.push_back(dist);
  }

  for (unsigned int i = 0; i < sides.size(); i++) {
    for (unsigned int j = i + 1; j < sides.size(); j++) {
      if (EqualSides(sides[i], sides[j])) return 1;
    }
  }
  return 0;
}

IsoscelesTriangle::IsoscelesTriangle(const std::string &name,
                                      const std::vector<Point> &points) 
    : Triangle(name, points) {
  if (!this->checkIsosceles()) {
    throw 1;
  }
}

EquilateralTriangle::EquilateralTriangle(const std::string &name,
                                          const std::vector<Point> &points)
    : Triangle(name, points), IsoscelesTriangle(name, points) {
  if (!this->HasAllEqualSides()) {
    throw 1;
  }
}

RightTriangle::RightTriangle(const std::string &name,
                              const std::vector<Point> &points)
    : Triangle(name, points) {
  if (!this->HasARightAngle()) {
    throw 1;
  }
}

IsoscelesRightTriangle::IsoscelesRightTriangle(const std::string &name,
                                                const std::vector<Point> &points)
    : Triangle(name, points),
    RightTriangle(name, points),
    IsoscelesTriangle(name, points) {
  if (!this->HasARightAngle() || !this->checkIsosceles()) {
    throw 1;
  }
}
