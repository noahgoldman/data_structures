#ifndef QUADRILATERAL_H_
#define QUADRILATERAL_H_

#include <string>
#include <vector>
#include "utilities.h"
#include "polygons.h"

class Quadrilateral : public Polygon {
  public:
    Quadrilateral(const std::string &name, const std::vector<Point> &points);
};

class Trapezoid : virtual public Quadrilateral {
  public:
    Trapezoid(const std::string &name, const std::vector<Point> &points);

  protected:
    int countParallel() const;
    int countEqualPairs() const;
};

class Parallelogram : virtual public Trapezoid {
  public:
    Parallelogram(const std::string &name, const std::vector<Point> &points);
};

class IsoscelesTrapezoid : virtual public Trapezoid {
  public:
    IsoscelesTrapezoid(const std::string &name, const std::vector<Point> &points);
};

class Rectangle : virtual public Parallelogram, virtual public IsoscelesTrapezoid {
  public:
    Rectangle(const std::string &name, const std::vector<Point> &points);
  
    int HasARightAngle() const { return 1; }
};

class Kite : virtual public Quadrilateral {
  public:
    Kite(const std::string &name, const std::vector<Point> &points);

  protected:
    int checkKite() const;
};

class Rhombus : virtual public Parallelogram, virtual public Kite {
  public:
    Rhombus(const std::string &name, const std::vector<Point> &points);
};

class Square : public Rectangle, public Rhombus {
  public:
    Square(const std::string &name, const std::vector<Point> &points);
};

#endif // QUADRILATERAL_H_
