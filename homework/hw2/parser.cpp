#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>
#include "error.h"
#include "game.h"
#include <algorithm>
#include <cassert>

// Global variable for the max team name for the left side column
int max = 0;

// This function will search a team vector for a given name, and if it finds
//  it, it will give either a win or a loss based on the third parameter
//
//  This function returns false if no matching team is found
int find_and_add_win(std::vector<Team> &teams, std::string name, int win) {
  unsigned int i;
  for (i = 0; i < teams.size(); i++) {
    std::string current_name = teams[i].getName();
    if (current_name == name) {
      if (win) teams[i].addWin();
      else teams[i].addLoss();
      return 1;
    }
  }
  return 0;
}

// Algorithm to sort games
int sort_games(const Game &game1, const Game &game2) {
  int diff1 = game1.getDifferential();
  int diff2 = game2.getDifferential();
  if (diff1 == diff2) {
    int t1 = game1.getTotal();
    int t2 = game2.getTotal();
    if (t1 == t2) {
      std::string v1 = game1.getVisitorName();
      std::string v2 = game2.getVisitorName();
      if (v1 == v2) {
        std::string h1 = game1.getHomeName();
        std::string h2 = game2.getHomeName();
        return (h1 < h2);
      }
      else {
        return (v1 < v2);
      }
    }
    else if (t1 > t2) return 1;
    return 0;
  }
  else if (diff1 < diff2) {
    return 1;
  }
  else {
    return 0;
  }
}

// Compare floats the the correct amount of precision
int float_same(float a, float b) {
  return (fabs(a - b) < 0.01);
}

int sort_teams(const Team &team1, const Team &team2) {
  float p1 = team1.getWinPercentage();
  float p2 = team2.getWinPercentage();
  if (float_same(p1, p2)) {
    int t1 = team1.getTotalGames();
    int t2 = team2.getTotalGames();
    if (t1 == t2) {
      return (team1.getName() < team2.getName());
    }
    else if (t1 > t2) return 1;
    return 0;
  }
  else if (p1 > p2) {
    return 1;
  }
  else {
    return 0;
  }
}

// Read in the games to memory and calculate the max team name for the left column
void parse(char *filename, std::vector<Game>& games) {
  std::ifstream file(filename);
  if (!file) die("The file stream failed to open");

  Game game;
  while(game.Init(file)) {
    games.push_back(game);
    max = std::max(max, game.getVisitorNameLength() + 2);
    max = std::max(max, game.getHomeNameLength() + 2);
  }
}

// Output the first section
void print_games(std::ofstream& outfile, std::vector<Game> &games) {
  outfile << "ALL GAMES, SORTED BY POINT DIFFERENTIAL:" << std::endl;

  std::sort(games.begin(), games.end(), sort_games);
  
  unsigned int i;
  int visitor_length = 0, home_length = 0;
  for (i = 0; i < games.size(); i++) {
    visitor_length = std::max(visitor_length, games[i].getVisitorNameLength() + 2);
    home_length = std::max(home_length, games[i].getHomeNameLength() + 2);
  }

  for (i = 0; i < games.size(); i++) {
    games[i].print(outfile, max, home_length);
  }
}

// Generate a new set of team objects that represent the entire team from all games
void generate_teams(std::vector<Game> &games, std::vector<Team> &teams) {
  unsigned int i;
  for (i = 0; i < games.size(); i++) {
    if (games[i].checkWinner()) {
      if(!find_and_add_win(teams, games[i].getVisitorName(), 1)) {
        Team team;
        team.setName(games[i].getVisitorName());
        team.addWin();
        teams.push_back(team);
      }
      if(!find_and_add_win(teams, games[i].getHomeName(), 0)) {
        Team team;
        team.setName(games[i].getHomeName());
        team.addLoss();
        teams.push_back(team);
      }
    }
    else {
      if(!find_and_add_win(teams, games[i].getVisitorName(), 0)) {
        Team team;
        team.setName(games[i].getVisitorName());
        team.addLoss();
        teams.push_back(team);
      }
      if(!find_and_add_win(teams, games[i].getHomeName(), 1)) {
        Team team;
        team.setName(games[i].getHomeName());
        team.addWin();
        teams.push_back(team);
      }
    }
  }
}

// Generate team objects for the extra section, based on second half victories
void generate_2nd_teams(std::vector<Game> games, std::vector<Team> &teams) {
  unsigned int i;
  for (i = 0; i < games.size(); i++) {
    if (games[i].checkVisitor2ndHalf()) {
      if (!find_and_add_win(teams, games[i].getVisitorName(), 1)) {
        Team team;
        team.setName(games[i].getVisitorName());
        team.addWin();
        teams.push_back(team);
      }
    }
    else {
      if (!find_and_add_win(teams, games[i].getVisitorName(), 0)) {
        Team team;
        team.setName(games[i].getVisitorName());
        team.addLoss();
        teams.push_back(team);
      }
    }
      
    if (games[i].checkHome2ndHalf()) {
      if (!find_and_add_win(teams, games[i].getHomeName(), 1)) {
        Team team;
        team.setName(games[i].getHomeName());
        team.addWin();
        teams.push_back(team);
      }
    }
    else {
      if (!find_and_add_win(teams, games[i].getHomeName(), 0)) {
        Team team;
        team.setName(games[i].getHomeName());
        team.addLoss();
        teams.push_back(team);
      }
    }
  }
}


void print_percentages(std::ofstream& outfile, std::vector<Team> &teams, std::string message, int extra) {
  outfile << std::endl;
  outfile << message << std::endl;

  std::sort(teams.begin(), teams.end(), sort_teams);

  unsigned int i;
  int team_length = 0;
  for (i = 0; i < teams.size(); i++) {
    team_length = std::max(team_length, teams[i].getNameLength() + 2);
  }

  for (i = 0; i < teams.size(); i++) {
    if (extra) teams[i].print_extra(outfile, max);
    else teams[i].print(outfile, max);
  }
}

int main(int argc, char *argv[]) {
  if (argc != 3) die("Usage: ./script infile outfile");

  std::vector<Game> games;
  parse(argv[1], games);

  std::ofstream outfile(argv[2]);

  print_games(outfile, games);

  std::string win_message = "ALL TEAMS, SORTED BY WIN PERCENTAGE:"; 
  std::string extra_message = "ALL TEAMS, SORTED BY PERCENTAGE OF GAMES WON AFTER LOSING AT HALFTIME:";

  std::vector<Team> teams;
  std::vector<Team> teams_extra;
  generate_teams(games, teams);
  generate_2nd_teams(games, teams_extra);
  print_percentages(outfile, teams, win_message, 0);
  print_percentages(outfile, teams_extra, extra_message, 1);

  return 0;
}
