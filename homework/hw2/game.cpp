#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>
#include "game.h"
#include "team.h"
#include "strtoint.h"
#include <math.h>

int Game::Init(std::istream& file) {
  std::string day_, year_;
  if (!(file >> this->day_name >> this->month >> day_ >> year_)) return 0;

  strtoint(day_.c_str(), &this->day);
  strtoint(year_.c_str(), &this->year);

  this->visitor.Init(file);
  this->home.Init(file);

  return 1;
}

const int Game::checkWinner() const {
  return (this->visitor.getTotal() > this->home.getTotal());
}

const int Game::getDifferential() const {
  return fabs(this->visitor.getTotal() - this->home.getTotal());
}

const int Game::getTotal() const {
  return (this->visitor.getTotal() + this->home.getTotal());
}

const int Game::getMaxNameLength() const {
  return std::max(this->visitor.getNameLength(), this->home.getNameLength());
}

const int Game::getVisitorNameLength() const {
  return this->visitor.getNameLength();
}

const int Game::getHomeNameLength() const {
  return this->home.getNameLength();
}

const std::string Game::prependSpace(int time) const {
  std::string output;
  std::stringstream out;
  out << time;
  output = out.str();
  if (time < 10) {
    output = " " + output; 
  }

  return output;
}

const std::string Game::getScoreString() const {
  std::string result;
  std::string visitor_score = prependSpace(this->visitor.getTotal());
  std::string home_score = prependSpace(this->home.getTotal()); 
  
  result = visitor_score + " - " + home_score;
  return result;
}

void Game::print(std::ofstream& outfile, int visitor_length, int home_length) const {
  std::string result;
  if (this->visitor.getTotal() > this->home.getTotal()) {
    result = "defeated  ";
  }
  else {
    result = "lost to   ";
  }

  outfile << this->visitor.getName() << std::string(visitor_length - this->visitor.getNameLength(), ' ')
    << result << this->home.getName() << std::string(home_length - this->home.getNameLength(), ' ')
    << this->getScoreString() << std::endl;
}

const std::string Game::getVisitorName() const {
  return this->visitor.getName();
}

const std::string Game::getHomeName() const {
  return this->home.getName();
}

const int Game::checkVisitor2ndHalf() const {
  if ((this->visitor.getQ2() < this->home.getQ2()) && this->checkWinner()) {
    return 1;
  }
  return 0;
}

const int Game::checkHome2ndHalf() const {
  if ((this->home.getQ2() < this->visitor.getQ2()) && !this->checkWinner()) {
    return 1;
  }
  return 0;
}
