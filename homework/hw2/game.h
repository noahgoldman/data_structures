#ifndef GAME_H_
#define GAME_H_
#include <iostream>
#include "team.h"


class Game {
  public:
    int Init(std::istream& file);
    const int checkWinner() const;
    const int getTotal() const;
    const int getDifferential() const;
    const int getMaxNameLength() const;
    const int getVisitorNameLength() const;
    const int getHomeNameLength() const;
    const std::string prependSpace(int) const;
    const std::string getScoreString() const;
    void print(std::ofstream& outfile, int visitor_length, int home_length) const;

    const std::string getVisitorName() const;
    const std::string getHomeName() const;

    const int checkVisitor2ndHalf() const;
    const int checkHome2ndHalf() const;

  private:
    std::string day_name;
    std::string month;
    int day;
    int year;

    Team visitor;
    Team home;
};

#endif
