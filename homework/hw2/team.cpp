#include <string>
#include <iostream>
#include <assert.h>
#include <sstream>
#include <iomanip>
#include <fstream>
#include "team.h"
#include "strtoint.h"

std::string prependSpace(int i);

int Team::Init(std::istream& file) {
  std::string q1_, q2_, q3_, q4_, ot_, total_;
  if (!(file >> this->name >> q1_ >> q2_ >> q3_ >> q4_ >> ot_ >> total_)) return 0;
 
  strtoint(q1_.c_str(), &this->q1);
  strtoint(q2_.c_str(), &this->q2);
  strtoint(q3_.c_str(), &this->q3);
  strtoint(q4_.c_str(), &this->q4);
  strtoint(ot_.c_str(), &this->ot);
  strtoint(total_.c_str(), &this->total);

  assert(this->q1 + this->q2 + this->q3 + this->q4 + this->ot == this->total);
  return 1;
}

void Team::setName(std::string name) {
  this->name = name;
  this->wins = 0;
  this->losses = 0;
}

const int Team::getTotal() const {
  return this->total;
}

const std::string Team::getName() const {
  return this->name;
}

const int Team::getNameLength() const {
  return this->name.length();
}

void Team::addWin() {
  this->wins += 1;
}

void Team::addLoss() {
  this->losses += 1;
}

int Team::getTotalGames() const {
  return (this->wins + this->losses);
}

float Team::getWinPercentage() const {
  return ((float) this->wins) / (((float)this->wins) + ((float)this->losses));
}

void Team::print(std::ofstream& outfile, int team_length) {
  outfile << std::fixed << std::setprecision(2) 
    << this->name << std::string(team_length - this->getNameLength(), ' ')
    << prependSpace(this->wins) << " win(s) - " << prependSpace(this->losses) << " loss(es)  "
    << this->getWinPercentage() << std::endl;
} 

void Team::print_extra(std::ofstream& outfile, int team_length) {
  outfile << std::fixed << std::setprecision(2) 
    << this->name << std::string(team_length - this->getNameLength(), ' ')
    << prependSpace(this->wins) << " win(s) - " << prependSpace(this->losses) << " game(s)  "
    << this->getWinPercentage() << std::endl;
} 

std::string prependSpace(int i) {
  std::string output;
  std::stringstream out;
  out << i;
  output = out.str();
  if (i < 10) {
    output = " " + output; 
  }

  return output.c_str();
}

const int Team::getQ2() const {
  return this->q2;
}
