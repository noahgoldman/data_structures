#ifndef TEAM_H_
#define TEAM_H_

class Team {
  public:
    int Init(std::istream& file);
    void setName(std::string name);
    const int getTotal() const;
    const std::string getName() const;
    const int getNameLength() const;

    void addWin();
    void addLoss();
    int getTotalGames() const;

    float getWinPercentage() const;

    void print(std::ofstream& outfile, int team_length);
    void print_extra(std::ofstream& outfile, int team_length);

    const int getQ2() const;

  private:
    std::string name;
    int q1;
    int q2;
    int q3;
    int q4;
    int ot;
    int total;

    int wins;
    int losses;
};

#endif
