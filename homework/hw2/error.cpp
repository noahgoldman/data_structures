#include <iostream>
#include "error.h"
#include <stdlib.h>

void die(const char *error) {
  std::cerr << "[ERROR]" << error << std::endl;
  exit(1);
}
