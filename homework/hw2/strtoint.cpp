#include <stdlib.h>
#include <iostream>
#include "error.h"
#include "strtoint.h"

void strtoint(const char *str, int *i) {
  char *endptr;
  int output = strtol(str, &endptr, 10);
  if (*endptr == '\0' && *str != '\0') {
    if (output < 0) die("The shape cannot have a negative height");
    *i = output;
  }
  else {
    die("this is not an integer");
  }
}
