HOMEWORK 7: WORD FREQUENCY MAPS


NAME: Noah Goldman


COLLABORATORS:  


ANALYSIS OF PERFORMANCE OF YOUR ALGORITHM:
(order notation & concise paragraph, < 200 words)

n = total number of words in the sample text file
m = number of unique words in the file
w = width of the sequencing window
p = average number of words observed to follow a particular word

How much memory will the map data structure require, in terms of n, m,
w, and p (order notation for memory use)?

The data structure will store a map for every unique word times the average number of words observed to follow itto the window'th power.  It can be written as: m+p^w 


What is the order notation for performance (running time) of each of
the commands?

load: O(n*w)

print: O(p^w+p^2)

generate most_common: O(p^w+p^2)

generate random: O(p^w)


EXTRA CREDIT

My implementation supports any window size, even though I didn't seem to get credit for it on the homework server.  The quality for outputted sentences seemed to get better up until about window size 4 or 5, after which it started to become worse.


MISC. COMMENTS TO GRADER:  
(optional, please be concise!)

I understand that I did not get credit for implementing variable window size from the homework server, but I have tested it locally and it certainly works.  Even though my code isn't very well organized in the main function, I hope it is clear that it will work with window values > 3.
