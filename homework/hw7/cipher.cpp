#include <assert.h>
#include <list>
#include <vector>
#include <string>
#include <map>
#include <iostream>
#include "cipher.h"

Cipher::Cipher() : value(0), window(0) {
  this->Init();
}

void Cipher::add(unsigned int i, std::list<std::string>::iterator key) {
  if (i < this->window) {
    Cipher *child = new Cipher(); 
    child->setWindow(this->window);
    std::pair<std::map<std::string, Cipher*>::iterator, bool> res =
      this->children->insert(std::make_pair(*key, child));
    
    ++key;
    assert(this->value < 1);
    res.first->second->add(i + 1, key);

    // Delete the child that didn't get inserted
    if (res.first->second != child) {
      delete child;
    }
  }
  else {
    this->increment();
  }
}

void Cipher::getFreq(unsigned int i, std::vector<std::string> &keys, std::vector<std::string> &words,
                      std::map<std::vector<std::string>, int> *out) {
  if (i < keys.size()) {
    std::map<std::string, Cipher*>::iterator head = this->children->find(keys[i]);
    //assert(head != this->children->end());
    if (head != this->children->end()) {
      assert(head->first == keys[i]);
      words.push_back(keys[i]);

      head->second->getFreq(i + 1, keys, words, out);
    }
  } else if (i == this->window) {
    assert(i > 1);
    out->operator[](words) = this->value;
  } else {
    for (std::map<std::string, Cipher*>::iterator itr = this->children->begin();
       itr != this->children->end(); itr++) {
      words.push_back(itr->first); 
      itr->second->getFreq(i + 1, keys, words, out);
      words.pop_back();
    }
  }
}

void Cipher::Init() {
  this->children = new std::map<std::string, Cipher*>;
}

void Cipher::Delete() {
  if (this->children != NULL) {
    for (std::map<std::string, Cipher*>::iterator itr = this->children->begin();
        itr != children->end(); itr++) {
      delete itr->second;
    }
    delete this->children;
  }
}
