#ifndef CIPHER_H_
#define CIPHER_H_

class Cipher {
  public:
    Cipher();
    ~Cipher() { this->Delete(); }

    void add(unsigned int i, std::list<std::string>::iterator key);
    void getFreq(unsigned int i, std::vector<std::string> &keys, std::vector<std::string> &words,
                  std::map<std::vector<std::string>, int> *out);

    inline void increment() { this->value++; }
    inline int isValue() const { if (value > 0) return 1; return 0; }
    inline void setWindow(int w) { this->window = w; }
    
    inline unsigned int getWindow() { return this->window; }

  private:
    void Init();
    void Delete();

    int value;
    unsigned int window;
    std::map<std::string, Cipher*> *children;
};

#endif
