// -----------------------------------------------------------------
// HOMEWORK 7 WORD FREQUENCY MAPS
//
// You may use all of, some of, or none of the provided code below.
// You may edit it as you like (provided you follow the homework
// instructions).
// -----------------------------------------------------------------

#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <vector>
#include <map>
#include "cipher.h"
#include "mersenne_twister.h"


void print_words(const std::list<std::string> &words) {
  std::cerr << "Words: ";
  for (std::list<std::string>::const_iterator itr = words.begin();
      itr != words.end(); itr++) {
    std::cerr << *itr << ", ";
  }
  std::cerr << "\n" << std::endl;
}

void print_words(const std::vector<std::string> &words) {
  std::cerr << "Words: " << words.front();
  for (unsigned int i = 1; i < words.size(); i++) {
    std::cerr << " " << words[i];
  }
  std::cerr << "\n" << std::endl;
}

// ASSIGNMENT: FILL IN YOUR OWN MAP STRUCTURE
typedef Cipher* MY_MAP;



// Custom helper function that reads the input stream looking for
// double quotes (a special case delimiter needed below), and white
// space.  Contiguous blocks of alphabetic characters are lowercased &
// packed into the word.
bool ReadNextWord(std::istream &istr, std::string &word) {
  char c;
  word.clear();
  while (istr) {
    // just "peek" at the next character in the stream
    c = istr.peek();
    if (isspace(c)) {
      // skip whitespace before a word starts
      istr.get(c);
      if (word != "") {
	// break words at whitespace
	return true;
      }
    } else if (c == '"') {
      // double quotes are a delimiter and a special "word"
      if (word == "") {
	istr.get(c);
	word.push_back(c);
      }
      return true;
    } else if (isalpha(c)) {
      // this a an alphabetic word character
      istr.get(c);
      word.push_back(tolower(c));
    } else {
      // ignore this character (probably punctuation)
      istr.get(c);
    }
  }
  return false;
}


// Custom helper function that reads the input stream looking a
// sequence of words inside a pair of double quotes.  The words are
// separated by white space, but the double quotes might not have
// space between them and the neighboring word.  Punctuation is
// ignored and words are lowercased.
std::vector<std::string> ReadQuotedWords(std::istream &istr) {
  // returns a vector of strings of the different words
  std::vector<std::string> answer;
  std::string word;
  bool open_quote = false;
  while (ReadNextWord(istr,word)) {
    if (word == "\"") {
      if (open_quote == false) { open_quote=true; }
      else { break; }
    } else {
      // add each word to the vector
      answer.push_back(word);
    }
  }
  return answer;
}


void add_words(const std::string &word, std::list<std::string> &words, Cipher &head) {
  words.pop_front();
  words.push_back(word);
  head.add(0, words.begin());
}

int main () {

  // ASSIGNMENT: THE MAIN DATA STRUCTURE
  Cipher head;

  // Parse each command
  std::string command;    
  while (std::cin >> command) {

    // load the sample text file
    if (command == "load") {
      std::string filename;
      int window;
      std::string parse_method;
      std::cin >> filename >> window >> parse_method;      

      head.setWindow(window);

      std::ifstream file;
      file.open(filename.c_str());
      assert(file.is_open());

      std::cout << "Loaded " << filename << " with window = " << window << " and parse method = "
        << parse_method << "\n" << std::endl;

      std::list<std::string> words;
      std::string word;
      int i = 0;
      while (i < window && ReadNextWord(file, word)) {
        if (word != "\"" || word != ",") {
          words.push_back(word);
          i++;
        }
      }
      
      head.add(0, words.begin());
      while (ReadNextWord(file, word)) {
        if (word != "\"") {
          add_words(word, words, head);
        }
      }

      for (int i = 0; i < (window); i++) {
        words.pop_front();
        words.push_back("");
        head.add(i, words.begin());
      }

    } 

    // print the portion of the map structure with the choices for the
    // next word given a particular sequence.
    else if (command == "print") {
      std::vector<std::string> sentence = ReadQuotedWords(std::cin);
      std::map<std::vector<std::string>, int> freqs;
      std::vector<std::string> words;
      head.getFreq(0, sentence, words, &freqs);
  
      if (freqs.size() != 0) {
      int count = 0;
      for (std::map<std::vector<std::string>, int>::iterator itr = freqs.begin();
          itr != freqs.end(); itr++) {
        count += itr->second;
      }

      for (std::map<std::vector<std::string>, int>::iterator itr = freqs.begin();
          itr != freqs.end(); itr++) {
        for (std::map<std::vector<std::string>, int>::iterator itr2 = itr;
            itr2 != freqs.end(); itr2++) {
          if (itr2 != itr && itr->first[sentence.size()] == itr2->first[sentence.size()]) {
            freqs.erase(itr2);
            itr->second += 1;
            itr2 = itr;
          }
        }
      } 

      for (unsigned int i = 0; i < sentence.size(); i++) {
        std::cout << sentence[i] << " ";
      }
      std::cout << "(" << count << ")" << std::endl;

      for (std::map<std::vector<std::string>, int>::iterator itr = freqs.begin();
          itr != freqs.end(); itr++) {
        if (itr->first.back() != "") {
          std::cout << itr->first.front();
          for (unsigned int i = 1; i <= sentence.size(); i++) {
            std::cout << " " << itr->first[i]; 
          }
          std::cout << " (" << itr->second << ")" << std::endl;
        }
      }

      std::cout << std::endl;
      }
    }

    // generate the specified number of words 
    else if (command == "generate") {
      std::vector<std::string> sentence = ReadQuotedWords(std::cin);
      // how many additional words to generate
      int length;
      std::cin >> length;
      std::string selection_method;
      std::cin >> selection_method;
      bool random_flag;

      unsigned int window = head.getWindow();
      std::vector<std::string> out;
      for (unsigned int i = 0; i <= (length - window) + 1; i++) {
        // Copy the data into a new vector of size "window" to
        // find the next word
 
        std::vector<std::string> subset;
        if (sentence.size() < window) {
          for (unsigned int j = 0; j < sentence.size() && j < window - 1; j++) {
            subset.push_back(sentence[j]);
          }
        } else {
          for (unsigned int j = i; j < sentence.size() && j < window + i - 1; j++) {
            subset.push_back(sentence[j]);
          }
        }

        std::vector<std::string> words;
        std::map<std::vector<std::string>, int> freqs;
        head.getFreq(0, subset, words, &freqs);

        if (selection_method == "most_common" && freqs.size() > 0) {
          // Find the greatest frequency word
          int max = 0;
          std::map<std::vector<std::string>, int>::const_iterator out_subset;
          for (std::map<std::vector<std::string>, int>::const_iterator itr = freqs.begin();
              itr != freqs.end(); itr++) {
            if (itr->second == max) {
              if (itr->first.back() < out_subset->first.back()) {
                max = itr->second;
                out_subset = itr;
              }
            } else if (itr->second > max && itr->first.back() != "") {
              max = itr->second;
              out_subset = itr;
            }
          }
             
          if (out.size() == 0) {
            // Add in all of the words if its the first iteration
            for (unsigned int j = 0; j < out_subset->first.size(); j++) {
              out.push_back(out_subset->first[j]);
              if (j != 0) {
                sentence.push_back(out_subset->first[j]);
              }
            }
          } else {
            out.push_back(out_subset->first.back());
            sentence.push_back(out_subset->first.back());
          }
        } else if (selection_method == "random" && freqs.size() > 0) {
          std::vector<std::vector<std::string> > choices;
          for (std::map<std::vector<std::string>, int>::const_iterator itr = freqs.begin();
              itr != freqs.end(); itr++) {
            for (int k = 0; k < itr->second; k++) {
              choices.push_back(itr->first);
            }
          }

          MTRand gen;
          int p = gen.randInt(choices.size() - 1);
          std::vector<std::string> choice = choices[p];
          if (out.size() == 0) {
            for (unsigned int j = 0; j < choice.size(); j++) {
              out.push_back(choice[j]);
              if (j != 0) {
                sentence.push_back(choice[j]);
              }
            }
          } else {
            sentence.push_back(choice.back());
            out.push_back(choice.back());
          }
        } else {
          i = (length - window) + 2;
        }
      }

      std::cout << out.front();
      for (unsigned int i = 1; i < out.size(); i++) {
        std::cout << " " << out[i];
      }
      std::cout << "\n" << std::endl;

    } else if (command == "quit") {
      break;
    } else {
      std::cout << "WARNING: Unknown command: " << command << std::endl;
    }
  }
}
