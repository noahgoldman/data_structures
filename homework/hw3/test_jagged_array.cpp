#include <stdio.h>
#include "jagged_array.h"
#include "debug.h"

int tests_run = 0;

static const char* test_num_bins() {
  JaggedArray<char> ja(4);
  mu_assert("number of bins not equal to 4", ja.numBins() == 4);
  return 0;
}

static const char* test_alternate_template() {
  JaggedArray<int> ja(10);
  mu_assert("using an int as a template causes an incorrect bin number", 
      ja.numBins() == 10);
  return 0;
}

static const char* test_is_packed() {
  JaggedArray<char> ja(5);
  mu_assert("The array is not packed after being constructed", !ja.isPacked());
  return 0;
}

static const char* test_add_element_char() {
  JaggedArray<char> ja(5);
  ja.addElement(1, 'a');
  ja.addElement(1, 'b'); 
  ja.addElement(1, 'c'); 
  ja.addElement(3, 'd'); 
  ja.addElement(3, 'e'); 
  ja.addElement(4, 'r'); 

  mu_assert("The number of elements is incorrect while adding elements", 
      ja.numElements() == 6);

  mu_assert("The number of elements in a bin is wrong", 
      ja.numElementsInBin(1) == 3);
  mu_assert("The number of elements in a bin is wrong", 
      ja.numElementsInBin(3) == 2);
  return 0;
}

static const char* test_get_element() {
  JaggedArray<char> ja(5);
  ja.addElement(1, 'a');
  ja.addElement(1, 'b'); 
  ja.addElement(1, 'c'); 
  ja.addElement(3, 'd'); 
  ja.addElement(3, 'e'); 
  ja.addElement(4, 'r'); 

  mu_assert("Getting elements fails", ja.getElement(1, 0) == 'a');
  mu_assert("Getting elements fails", ja.getElement(3, 1) == 'e');
  mu_assert("Getting elements fails", ja.getElement(4, 0) == 'r');
  return 0;
}

static const char* test_remove_element() {
  JaggedArray<char> ja(5);
  ja.addElement(1, 'a');
  ja.addElement(1, 'b'); 
  ja.addElement(1, 'c'); 
  ja.addElement(3, 'd'); 
  ja.addElement(3, 'e'); 
  ja.addElement(4, 'r'); 

  mu_assert("Number before removal fails", ja.numElementsInBin(1) == 3);
  ja.removeElement(1, 2);
  mu_assert("Number after removal fails", ja.numElementsInBin(1) == 2);
  return 0;
}

static const char* test_clear() {
  JaggedArray<char> ja(5);
  ja.addElement(1, 'a');
  ja.addElement(1, 'b'); 
  ja.addElement(1, 'c'); 
  ja.addElement(3, 'd'); 
  ja.addElement(3, 'e'); 
  ja.addElement(4, 'r'); 

  mu_assert("Number before clear fails", ja.numElements() == 6);
  ja.clear();
  mu_assert("Number after clear fails", ja.numElements() == 0);
  return 0;
}

static const char* test_pack() {
  JaggedArray<char> ja(5);
  ja.addElement(1, 'a');
  ja.addElement(1, 'b'); 
  ja.addElement(1, 'c'); 
  ja.addElement(3, 'd'); 
  ja.addElement(3, 'e'); 
  ja.addElement(4, 'r'); 

  mu_assert("array is not unpacked", !ja.isPacked());
  ja.pack();
  mu_assert("The number of elements is incorrect while packing", 
      ja.numElements() == 6);

  mu_assert("The number of elements in a bin is wrong after packing", 
      ja.numElementsInBin(1) == 3);
  mu_assert("The number of elements in a bin is wrong after packing", 
      ja.numElementsInBin(3) == 2);
  
  mu_assert("The array is not packed", ja.isPacked());
  
  mu_assert("The array doesn't have the right elements",
      ja.getElement(1, 1) == 'b');

  return 0;
}

static const char* test_unpack() {
  JaggedArray<char> ja(5);
  ja.addElement(1, 'a');
  ja.addElement(1, 'b'); 
  ja.addElement(1, 'c'); 
  ja.addElement(3, 'd'); 
  ja.addElement(3, 'e'); 
  ja.addElement(4, 'r'); 

  ja.pack();
  mu_assert("The array is not packed", ja.isPacked());
  ja.unpack();
  mu_assert("The array is not unpacked", !ja.isPacked());
  mu_assert("The number of elements is incorrect while packing", 
    ja.numElements() == 6);

  mu_assert("The number of elements in a bin is wrong after packing", 
    ja.numElementsInBin(1) == 3);
  mu_assert("The number of elements in a bin is wrong after packing", 
    ja.numElementsInBin(3) == 2);
  
  mu_assert("The array doesn't have the right elements",
    ja.getElement(1, 1) == 'b');
  return 0;
}

static const char* all_tests() {
  mu_run_test(test_num_bins);
  mu_run_test(test_alternate_template);
  mu_run_test(test_is_packed);
  mu_run_test(test_add_element_char);
  mu_run_test(test_get_element);
  mu_run_test(test_remove_element);
  mu_run_test(test_clear);
  mu_run_test(test_pack);
  mu_run_test(test_unpack);
  return 0;
}

int main(int argc, char *argv[]) {
  const char *result = all_tests();
  if (result != 0) {
    printf("%s\n", result);
  } else {
    printf("ALL TESTS PASSED GG\n");
  }

  printf("Tests run: %d\n", tests_run);

  return result != 0;
}
