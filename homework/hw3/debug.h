#ifndef DIE_H_
#define DIE_H_

#include <iostream>
#include <stdlib.h>

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { const char *message = test(); tests_run++; \
                                 if (message) return message; } while (0)
extern int tests_run;

inline void die(const char *error) {
  std::cerr << "[ERROR]" << error << std::endl;
  exit(1);
}

#endif
