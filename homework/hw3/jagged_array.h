#ifndef HOMEWORK_HW3_JAGGED_ARRAY_H_
#define HOMEWORK_HW3_JAGGED_ARRAY_H_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

inline const int max(const int i1, const int i2) {
  if (i1 > i2) return i1;
  else if (i1 < i2) return i2;
  return i1;
}

inline void die(const char *error) {
  std::cerr << "[ERROR]" << error << std::endl;
  exit(1);
}

template <class T> class JaggedArray {
  public:
    explicit JaggedArray(const int bins) { this->Init(bins); }
    JaggedArray(const JaggedArray &ja) { this->Clone(ja); }
    JaggedArray& operator=(const JaggedArray &ja);
    ~JaggedArray() { this->Delete(); }

    // Accessors
    const int numElements() const;
    const int numBins() const;
    const int numElementsInBin(const int bin) const;
    const T getElement(const int bin, const int element) const;
    const bool isPacked() const;

    // Modifiers
    void addElement(const int bin, const T& val);
    void removeElement(const int bin, const int element);
    void clear();
    void pack();
    void unpack();

    void print();

  private:
    void Init(const int bins);
    void Clone(const JaggedArray &ja);
    void Delete();

    void printRow(const int index);
    void initZero(int *array, const int n);
    void initNull();
    void deleteUnpackedValues();

    int num_elements;
    int num_bins;
    int *counts;
    T **unpacked_values;
    int *offsets;
    T *packed_values;
};

template <class T> void JaggedArray<T>::Init(const int bins) {
  this->num_elements = 0;
  this->num_bins = bins;
  this->counts = new int[bins];
  this->unpacked_values = new T*[bins];
  this->offsets = NULL;
  this->packed_values = NULL;

  this->initZero(this->counts, this->num_bins);
  this->initNull();
}

template <class T> void JaggedArray<T>::Clone(const JaggedArray &ja) {
  this->num_bins = ja.num_bins;
  this->num_elements = ja.num_elements;
  if (ja.isPacked()) {
    this->counts = NULL;
    this->unpacked_values = NULL;
    this->offsets = new int[this->num_bins];
    this->packed_values = new T[this->num_elements];

    this->initZero(this->offsets, this->num_bins);

    for (int i = 0; i < this->num_bins; i++) {
      this->offsets[i] = ja.offsets[i];
    }
    for (int i = 0; i < this->num_elements; i++) {
      this->packed_values[i] = ja.packed_values[i];
    }
  } else {
    this->counts = new int[this->num_bins];
    this->unpacked_values = new T*[this->num_bins];
    this->offsets = NULL;
    this->packed_values = NULL;

    this->initZero(this->counts, this->num_bins);
    this->initNull();

    for (int i = 0; i < this->num_bins; i++) {
      this->counts[i] = ja.counts[i];
    }
    for (int i = 0; i < this->num_bins; i++) {
      if (this->counts[i] > 0) {
        this->unpacked_values[i] = new T[this->counts[i]];
        for (int j = 0; j < this->counts[i]; j++) {
          this->unpacked_values[i][j] = ja.unpacked_values[i][j];
        }
      }
    }
  }
}

template <class T> JaggedArray<T>& JaggedArray<T>::operator=(
                                                    const JaggedArray<T> &ja) {
  if (this != &ja) {
    this->Delete();
    this->Clone(ja);
  }
  return *this;
}

template <class T> void JaggedArray<T>::Delete() {
  if (!this->isPacked()) {
    this->deleteUnpackedValues();
  }
  delete [] this->counts;
  delete [] this->unpacked_values;
  delete [] this->offsets;
  delete [] this->packed_values;
}


template <class T> const int JaggedArray<T>::numElements() const {
  return this->num_elements;
}

template <class T> const int JaggedArray<T>::numBins() const {
  return this->num_bins;
}

template <class T> const int JaggedArray<T>::numElementsInBin(const int bin)
                                                              const {
  if (this->isPacked()) {
    if (bin == this->num_bins - 1)
      return (this->num_elements - this->offsets[bin]);
    return (this->offsets[bin + 1] - this->offsets[bin]);
  } else {
    return this->counts[bin];
  }
}

template <class T> const T JaggedArray<T>::getElement(const int bin,
                                                      const int element) const {
  if (bin >= this->num_bins) die("The intended bin is out of range");

  if (this->isPacked()) {
    if (element >= this->num_elements)
      die("The intended element is out of range");
    int bin_start = this->offsets[bin];
    return this->packed_values[bin_start + element];
  } else {
    if (element >= this->counts[bin])
      die("The intended element is out of range");
    return this->unpacked_values[bin][element];
  }
}

template <class T> const bool JaggedArray<T>::isPacked() const {
  return (this->counts == NULL && this->unpacked_values == NULL);
}

template <class T> void JaggedArray<T>::addElement(const int bin,
                                                    const T& val) {
  if (this->isPacked()) die("Can't edit a packed jagged array");
  if (bin >= this->num_bins) die("The intended bin is out of range");

  T* new_array = new T[this->counts[bin] + 1];

  for (int i = 0; i < this->counts[bin]; i++) {
    new_array[i] = this->unpacked_values[bin][i];
  }

  new_array[this->counts[bin]] = val;
  this->counts[bin] += 1;
  this->num_elements += 1;

  if (this->unpacked_values[bin] != NULL) {
    delete [] this->unpacked_values[bin];
  }
  this->unpacked_values[bin] = new_array;
}

template <class T> void JaggedArray<T>::removeElement(const int bin,
                                                      const int element) {
  if (this->isPacked()) die("Can't edit a packed jagged array");
  if (bin >= this->num_bins) die("The intended bin is out of range");

  if (this->counts[bin] > 1) {
    T* new_array = new T[this->counts[bin] - 1];

    int i, j;
    for (i = 0, j = 0; j < this->counts[bin]; i++, j++) {
      if (i == element) j++;
      new_array[i] = this->unpacked_values[bin][j];
    }

    --this->counts[bin];
    --this->num_elements;

    delete [] this->unpacked_values[bin];
    this->unpacked_values[bin] = new_array;
  } else {
    this->counts[bin] = 0;
    --this->num_elements;
    delete [] this->unpacked_values[bin];
    this->unpacked_values[bin] = NULL;
  }
}

template <class T> void JaggedArray<T>::clear() {
  if (this->isPacked()) die("Can't edit a packed jagged array");

  for (int i = 0; i < this->num_bins; i++) {
    if (this->counts[i] != 0) delete [] this->unpacked_values[i];
    this->unpacked_values[i] = NULL;
    this->counts[i] = 0;
  }

  this->num_elements = 0;
}

template <class T> void JaggedArray<T>::pack() {
  if (this->isPacked()) die("Can't pack a packed jagged array");
  int i, j;
  int k = 0;

  this->offsets = new int[this->num_bins];
  this->packed_values = new T[this->num_elements];

  this->initZero(this->offsets, this->num_bins);

  for (i = 0; i < this->num_bins; i++) {
    this->offsets[i] = k;
    for (j = 0; j < this->counts[i]; j++) {
      this->packed_values[k] = this->unpacked_values[i][j];
      k = k + 1;
    }
  }

  delete [] this->counts;
  this->deleteUnpackedValues();
  delete [] this->unpacked_values;
  this->counts = NULL;
  this->unpacked_values = NULL;
}

template <class T> void JaggedArray<T>::unpack() {
  if (!this->isPacked()) die("The jagged array is already packed");

  this->counts = new int[this->num_bins];
  this->initZero(this->counts, this->num_bins);
  this->unpacked_values = new T*[this->num_bins];
  this->initNull();

  int i, j, k = 0;
  for (i = 0; i < this->num_bins && k < this->num_elements; i++) {
    int array_size;
    if (i == this->num_bins - 1) {
      array_size = this->num_elements - this->offsets[i];
    } else {
      array_size = this->offsets[i+1] - this->offsets[i];
    }
    if (array_size != 0) {
      T* array = new T[array_size];
      for (j = 0; j < array_size; j++) {
        array[j] = this->packed_values[k];
        k++;
      }
      this->unpacked_values[i] = array;
      this->counts[i] = array_size;
    }
  }

  delete [] this->packed_values;
  delete [] this->offsets;
  this->offsets = NULL;
  this->packed_values = NULL;
}

template <class T> void JaggedArray<T>::print() {
  char *status;
  if (this->isPacked()) {
    status = const_cast<char*>("packed");
  } else {
    status = const_cast<char*>("unpacked");
  }
  printf("JaggedArray is %s\n", status);

  printf("  Bins: %d\n", this->num_bins);
  printf("  Elements: %d\n", this->num_elements);

  if (this->isPacked()) {
    printf("  Offsets:");
    for (int i = 0; i < this->num_bins; i++) {
      printf(" %d", this->offsets[i]);
    }
    printf("\n");
    printf("  Values: ");
    for (int i = 0; i < this->num_elements; i++) {
      std::cout << " " << this->packed_values[i];
    }
    printf("\n\n");
  } else {
    printf("  Counts:");
    int bin_max = 0;
    for (int i = 0; i < this->num_bins; i++) {
      printf(" %d", this->counts[i]);
      bin_max = max(bin_max, this->counts[i]);
    }
    printf("\n");

    printf("  Values:");
    this->printRow(0);
    for (int i = 1; i < bin_max; i++) {
      printf("         ");
      this->printRow(i);
    }
    printf("\n");
  }
}

// Prints an individual row of characters for the unpacked array
template <class T> void JaggedArray<T>::printRow(const int index) {
  for (int i = 0; i < this->num_bins; i++) {
    if (index < this->counts[i]) {
      std::cout << " " << this->unpacked_values[i][index];
    } else {
      printf("  ");
    }
  }
  printf("\n");
}

// Initializes a given array to zeros
template <class T> void JaggedArray<T>::initZero(int *array, const int n) {
  for (int i = 0; i < n; i++) {
    array[i] = 0;
  }
}

// Initializes all bins to NULL
template <class T> void JaggedArray<T>::initNull() {
  for (int i = 0; i < this->num_bins; i++) {
    this->unpacked_values[i] = NULL;
  }
}

// Deletes each bin individually
template <class T> void JaggedArray<T>::deleteUnpackedValues() {
  for (int i = 0; i < this->num_bins; i++) {
    if (this->unpacked_values[i] != NULL) {
      delete [] this->unpacked_values[i];
    }
  }
}

#endif  // HOMEWORK_HW3_JAGGED_ARRAY_H_
