#ifndef DIE_H_
#define DIE_H_

#include <iostream>
#include <stdlib.h>

inline void die(const char *error) {
  std::cerr << "[ERROR]" << error << std::endl;
  exit(1);
}

inline void warn(const char *error) {
  std::cerr << "WARNING: " << error << std::endl;
}

#endif
