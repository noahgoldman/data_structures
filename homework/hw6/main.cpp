#include <iostream>
#include <fstream>
#include <cassert>
#include <cstdlib>

#include "robot.h"
#include "board.h"

// ======================================================================================
// helper functions

void ReadInput(std::istream &istr, Board &board, std::vector<Robot> &robots);
void print_usage_message(std::string program_name) {
    std::cerr << "Usage:  " << program_name << " <input.txt> [ -find_all_solutions ] [ -verbose ] [ -allow_pushing ]" << std::endl;
}
void pathfind(unsigned int i, std::vector<Robot> &robots, Board *board, std::vector<Board> *solutions); 

// ======================================================================================

bool verbose = false;
bool allow_pushing = false;
bool find_all_solutions = false;

int main(int argc, char* argv[]) {

  // print usage information
  if (argc < 2) {
    print_usage_message(argv[0]);
    exit(1);
  }

  // process the command line arguments
  for (int i = 2; i < argc; i++) {
    if (std::string(argv[i]) == "-find_all_solutions") {
      find_all_solutions = true;
    } else if (std::string(argv[i]) == "-allow_pushing") {
      allow_pushing = true;
    } else if (std::string(argv[i]) == "-verbose") {
      verbose = true;
    } else {
      std::cerr << "Unknown command line argument: " << argv[i] << std::endl; 
      print_usage_message(argv[0]);
      exit(1); 
    }
  }

  // open the input file
  std::ifstream istr(argv[1]);
  if (!istr) { 
    std::cerr << "Failed to open " << argv[1] << std::endl;
    exit(1);
  }

  // read the board & robot information
  Board board;
  std::vector<Robot> robots;
  ReadInput(istr,board,robots);


  for (unsigned int i = 0; i < robots.size(); i++) {
    board.addRobot(&(robots[i]), robots[i].getStartPosition(), robots[i].getStartDirection());
  }

  if (find_all_solutions) {
    std::cout << "Searching for all solutions..." << std::endl;
  } else {
    std::cout << "Searching for just one solution..." << std::endl;
  }
  std::vector<Board> *solutions = new std::vector<Board>;
  pathfind(0, robots, &board, solutions);

  for (std::vector<Board>::iterator itr = solutions->begin();
      itr != solutions->end(); itr++) {
    std::vector<Board>::iterator temp = itr;
    for (std::vector<Board>::iterator itr2 = ++temp;
        itr2 != solutions->end(); itr2++) {
      if (*itr == *itr2) {
        itr2 = solutions->erase(itr2);
        itr2--;
      }
    }
  }
    
  for (unsigned int i = 0; i < solutions->size(); i++) {
    std::cout << "Solution:" << std::endl;
    solutions->operator[](i).printSolutions();
    solutions->operator[](i).createBoards(allow_pushing);
  }
  if (solutions->size() == 0) {
    std::cout << "Found no solutions" << std::endl;
  } else {
    std::cout << "Found " << solutions->size() << " solution(s)" << std::endl;
  }
}


// ======================================================================================

// Parse the input file
void ReadInput(std::istream &istr, Board &board, std::vector<Robot> &robots) {

  // read in the board dimensions
  std::string token;
  int rows, cols;
  istr >> token >> cols >> rows;
  assert (token == "board");
  assert (rows >= 1 && cols >= 1);
  int num_steps;
  istr >> token >> num_steps;
  assert (token == "num_steps");
  assert (num_steps >= 1);

  // create the board
  board = Board(verbose, rows,cols,num_steps);
  
  // read in the number of robots & number of program steps
  int num_robots;
  istr >> token >> num_robots;
  assert (token == "num_robots");
  assert (num_robots >= 1);
  
  for (int i = 0; i < num_robots; i++) {

    // read in each robot
    char symbol;
    istr >> token >> symbol;
    assert (token == "robot");
    int start_row, start_col;
    std::string direction;
    istr >> token >> start_col >> start_row >> direction;
    assert (token == "start");
    assert (start_row >= 0 && start_row < rows);
    assert (start_col >= 0 && start_col < cols);
    int goal_row, goal_col;
    istr >> token >> goal_col >> goal_row;
    assert (token == "goal");
    assert (goal_row >= 0 && goal_row < rows);
    assert (goal_col >= 0 && goal_col < cols);

    // read the commands into a vector structure
    std::vector<std::string> *commands = new std::vector<std::string>;
    for (int j = 0; j < num_steps; j++) {
      istr >> token;
      commands->push_back(token);      
    }

    // create a new robot and add to the collection
    Position start(start_row, start_col);
    Position goal(goal_row, goal_col);
    Robot robot(symbol, start, direction, goal, commands);
    assert(robot.getGoalPosition() == goal);
    robots.push_back(robot);
  }
}

void print_commands(std::vector<std::string> *commands) {
  if (verbose) std::cout << "PRINTING COMMANDS:" << std::endl;
  assert(commands != NULL);
  if (commands->size() == 0) {
    if (verbose) std::cout << "The commands are empty" << std::endl;
  } else {
    assert(commands->begin() != commands->end());
    for (std::vector<std::string>::iterator itr = commands->begin();
        itr != commands->end(); itr++) {
      if (verbose) std::cout << *itr << std::endl;
    }
  }
  if (verbose) std::cout << std::endl;
}

int die = 0;
void pathfind(unsigned int i, std::vector<Robot> &robots, Board *board, std::vector<Board> *solutions) {
  // If we are at the start of the array, check for errors and pushes
  if (die) return;
  if (i == robots.size()) {
    i = 0;
  }
  if (i == 0) {
    if (!board->checkCollisions(allow_pushing)) {
      if (verbose) std::cout << "COLLISION" << std::endl;
      return;
    }
    if (board->outOfBounds()) {
      return;
    }
    //board->printBoards();
    if (board->checkGoals()) {
      if (verbose) std::cout << "GG SOLUTION FOUND" << std::endl;
      Board solution_board(*board);
      solutions->push_back(solution_board);

      if (!find_all_solutions) die = 1;
    }
  }
  if (verbose) std::cout << "Robot " << robots[i].getSymbol() << std::endl;
  std::vector<std::string> *commands = robots[i].getCommands();

  if (commands->size() > 0) {
    for (std::vector<std::string>::iterator itr = commands->begin();
        itr != commands->end(); itr++) {
      std::string cmd = *itr;
      board->move(&(robots[i]), cmd);
      itr = commands->erase(itr);
      int found = 0;
      for (std::vector<std::string>::iterator check = commands->begin();
          check != itr; check++) {
        if (*itr == *check) found = 1;
      }
      if (!found) {
        pathfind(i + 1, robots, board, solutions);
      }
      board->unmove(&(robots[i]), cmd);
      if (itr == commands->end()) {
        if (verbose) std::cout << "inserting back" << std::endl;
        commands->push_back(cmd);
      } else {
        if (commands->size() == 1) {
          itr = commands->begin();
        }
        assert(commands->size() >= 1);
        itr = commands->insert(itr, cmd);
      }
      print_commands(commands);
    }
  }
  if (verbose) std::cout << "time to return!" << std::endl;
  return;
}
