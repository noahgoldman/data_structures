#ifndef _BOARD_H_
#define _BOARD_H_

#include <vector>
#include <string>
#include <map>
#include "robot.h"

// The Board class stores the board dimensions, number of steps for
// each robot in the simulation, and prepares the output
class Board {
public:
  Board() : verbose(0), rows(0), cols(0), steps(0) {}
  Board(int v, int r, int c, int s);
  Board(const Board &b) { this->Clone(b); }

  inline int numRows() const { return rows; }
  inline int numCols() const { return cols; }

  void addRobot(const Robot *robot, const Position &start, const std::string &direction);
  void move(const Robot *robot, const std::string &command);
  void unmove(const Robot *robot, const std::string &command);

  void printSolutions() const;
  void printBoards() const;
  void createBoards(int push);

  int checkCollisions(int push);
  int outOfBounds();
  int checkGoals();

  bool operator==(const Board &b) const;
  
private:
  void Clone(const Board &board);
  void Init();

  int checkNotMove(const std::string &str);
  int checkPush(const Robot *r1, const Robot *r2);

  void addPastLocations(const Robot *robot, std::vector<Position> &locations);
  void print_map(std::map<const Robot*, Position> *cur_map);
  std::vector<std::string> print() const;

  int verbose;
  int rows;
  int cols;
  unsigned int steps;

  std::vector<std::vector<std::string> > *ascii_boards;
  std::map<const Robot*, Position> *positions;
  std::map<const Robot*, std::string> *directions;
  std::map<const Robot*, std::vector<std::string> > *moves;
};

#endif
