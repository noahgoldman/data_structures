HOMEWORK 6: ROBO RALLY RECURSION


NAME: Noah Goldman


TIME SPENT ON THIS ASSIGNMENT: 15


COLLABORATORS AND OTHER RESOURCES:
Eric Culp

ANALYSIS OF PERFORMANCE OF YOUR ALGORITHM:
(order notation & concise paragraph, < 200 words)

Since my algorithm aims to test every single possible combination of steps, the worst case scenario with each robot having no repeated steps would be O((r*s)!).  The easiest way to increase the time it takes for my algorithm to complete a certain solution is to either add robots or add steps per robot.  The dimensions of the board don't have any direct effect, although a smaller board will allow my algorithm to not recurse and skip trying a path where the robot would go off the board. Other operations such as inserting and pushing into vector and maps also effect the running time, but their impact is fairly insignificant compared to the order of the base recursion.


SUMMARY OF PERFORMANCE OF YOUR PROGRAM ON THE PROVIDED PUZZLES:

My program runs fairly instantaneously on all of the provided puzzles except for puzzle3.txt.  All of thsese can be fully solved (find_all_solutions) in less than 0.10 seconds.  Puzzle 3 is significantly slower, as the running time for a single solution is around 30 seconds and the time for all solutions takes about 5 minutes.  All of these times are from optimization level 3 (-O3) on gcc.



MISC. COMMENTS TO GRADER:  
(optional, please be concise!)


