#ifndef _ROBOT_H_
#define _ROBOT_H_

#include <string>
#include <vector>
#include <vector>
#include "position.h"

class Robot {
  public:
    Robot(char symbol_, const Position &start_position_, 
          const std::string &start_direction_, const Position &goal_position_,
          std::vector<std::string> *commands_);
    Robot(const Robot &old) { this->Clone(old); }
    ~Robot() { this->Delete(); };
    
    // ACCESSORS
    inline char getSymbol() const { return symbol; }
    inline std::vector<std::string>* getCommands() const { return commands; }
    inline const std::string& getStartDirection() const { return this->start_direction; }
    inline const Position& getStartPosition() const { return this->start_position; }
    inline const Position& getGoalPosition() const { return this->goal_position; }

  private:
    void Delete();
    void Clone(const Robot& old);

    char symbol;

    Position start_position;
    std::string start_direction;
    Position goal_position;

    std::vector<std::string> *commands;
};

#endif
