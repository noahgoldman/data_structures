#include <stdio.h>
#include "position.h"
#include "board.h"
#include "robot.h"
#include "gtest/gtest.h"

namespace {

//
// Test Position struct
//

TEST(PositionTest, Initialization) {
  Position pos(1, 2);
  EXPECT_EQ(1, pos.col);
  EXPECT_EQ(2, pos.row);
}

class RobotTest : public ::testing::Test {
  protected:
    RobotTest() : start(1, 1), goal(5, 6) {
      commands = new std::list<std::string>;
    }

    virtual void SetUp() {
      commands->push_back("rotate_right");
      commands->push_back("rotate_left");
      commands->push_back("rotate_left");
      commands->push_back("forward_2");
      commands->push_back("forward_1");
      commands->push_back("forward_1");

      start_dir = "west";

      symbol = 'a';
    }

    virtual void TearDown() {
      //delete commands;
    }

    std::list<std::string> *commands;
    char symbol;
    std::string start_dir;
    Position start;
    Position goal;
};

TEST_F(RobotTest, TestConstructor) {
  Robot r0(symbol, start, start_dir, goal, commands);
  EXPECT_EQ('a', r0.getSymbol()); 
  EXPECT_EQ("west", r0.getStartDirection());
  EXPECT_EQ(1, r0.getStartPosition().col);
  EXPECT_EQ(1, r0.getStartPosition().row);
  EXPECT_EQ(5, r0.getGoalPosition().col);
  EXPECT_EQ(6, r0.getGoalPosition().row);
}
  
} // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
