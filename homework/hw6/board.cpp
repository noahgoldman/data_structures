#include <iostream>
#include <cassert>
#include <string>
#include "board.h"
#include "debug.h"

void Board::print_map(std::map<const Robot*, Position> *cur_map) {
  for (std::map<const Robot*, Position>::iterator itr = cur_map->begin();
      itr != cur_map->end(); itr++) {
    if (this->verbose) std::cout << "Robot " << itr->first->getSymbol() << " is " << 
      itr->second << std::endl;
  }
}

Board::Board(int v = 0, int r = 0, int c = 0, int s = 0) : verbose(v), rows(r), cols(c), steps(s) {
  this->Init();
}

void Board::addRobot(const Robot *robot, const Position &start, const std::string &direction) {
  this->positions->insert(std::make_pair(robot, start));
  this->directions->insert(std::make_pair(robot, direction));
}

void Board::move(const Robot *robot, const std::string &command) {
  std::string current_direction = directions->operator[](robot);
  if (this->verbose) std::cout << "Moving:\nRobot " << robot->getSymbol() << " is now at " <<
    positions->operator[](robot) << " and its direction is " <<
    current_direction << std::endl;
  if (command == "rotate_right") {
    if (current_direction == "north") {
      directions->operator[](robot) = "east";
    } else if (current_direction == "east") {
      directions->operator[](robot) = "south";
    } else if (current_direction == "south") {
      directions->operator[](robot) = "west";
    } else if (current_direction == "west") {
      directions->operator[](robot) = "north";
    } 
  }
  else if (command == "rotate_left") {
   if (current_direction == "north") {
      directions->operator[](robot) = "west";
    } else if (current_direction == "east") {
      directions->operator[](robot) = "north";
    } else if (current_direction == "south") {
      directions->operator[](robot) = "east";
    } else if (current_direction == "west") {
      directions->operator[](robot) = "south";
    } 
  }
  else if (command == "u_turn") {
   if (current_direction == "north") {
      directions->operator[](robot) = "south";
    } else if (current_direction == "east") {
      directions->operator[](robot) = "west";
    } else if (current_direction == "south") {
      directions->operator[](robot) = "north";
    } else if (current_direction == "west") {
      directions->operator[](robot) = "east";
    } 
  }
  else if (command == "forward_1") {
    if (current_direction == "north") {
      ++positions->operator[](robot).row;
    } else if (current_direction == "east") {
      ++positions->operator[](robot).col;
    } else if (current_direction == "south") {
      --positions->operator[](robot).row;
    } else if (current_direction == "west") {
      --positions->operator[](robot).col;
    }
  } 
  else if (command == "forward_2") {
    if (current_direction == "north") {
      positions->operator[](robot).row += 2;
    } else if (current_direction == "east") {
      positions->operator[](robot).col += 2;
    } else if (current_direction == "south") {
      positions->operator[](robot).row -= 2;
    } else if (current_direction == "west") {
      positions->operator[](robot).col -= 2;
    }
  } 
  else if (command == "forward_3") {
    if (current_direction == "north") {
      positions->operator[](robot).row += 3;
    } else if (current_direction == "east") {
      positions->operator[](robot).col += 3;
    } else if (current_direction == "south") {
      positions->operator[](robot).row -= 3;
    } else if (current_direction == "west") {
      positions->operator[](robot).col -= 3;
    }
  }
  else if (command == "backward_1") {
    if (current_direction == "north") {
      positions->operator[](robot).row -= 1;
    } else if (current_direction == "east") {
      positions->operator[](robot).col -= 1;
    } else if (current_direction == "south") {
      positions->operator[](robot).row += 1;
    } else if (current_direction == "west") {
      positions->operator[](robot).col += 1;
    }
  }
  else if (command == "do_nothing") {}
  else {
    die("this command is not valid"); 
  }

  this->moves->operator[](robot).push_back(command);
  if (this->verbose) {
    std::cout << "The current path is: ";
    for (unsigned int i = 0; i < this->moves->operator[](robot).size(); i++) {
      std::cout << " -> " << this->moves->operator[](robot)[i];
    }
    std::cout << std::endl;
  }
}

void Board::unmove(const Robot *robot, const std::string &command) {
  std::string current_direction = directions->operator[](robot);
  if (command == "rotate_left") {
    if (current_direction == "north") {
      directions->operator[](robot) = "east";
    } else if (current_direction == "east") {
      directions->operator[](robot) = "south";
    } else if (current_direction == "south") {
      directions->operator[](robot) = "west";
    } else if (current_direction == "west") {
      directions->operator[](robot) = "north";
    } 
  }
  else if (command == "rotate_right") {
   if (current_direction == "north") {
      directions->operator[](robot) = "west";
    } else if (current_direction == "east") {
      directions->operator[](robot) = "north";
    } else if (current_direction == "south") {
      directions->operator[](robot) = "east";
    } else if (current_direction == "west") {
      directions->operator[](robot) = "south";
    } 
  }
  else if (command == "u_turn") {
   if (current_direction == "north") {
      directions->operator[](robot) = "south";
    } else if (current_direction == "east") {
      directions->operator[](robot) = "west";
    } else if (current_direction == "south") {
      directions->operator[](robot) = "north";
    } else if (current_direction == "west") {
      directions->operator[](robot) = "east";
    } 
  }
  else if (command == "backward_1") {
    if (current_direction == "north") {
      ++positions->operator[](robot).row;
    } else if (current_direction == "east") {
      ++positions->operator[](robot).col;
    } else if (current_direction == "south") {
      --positions->operator[](robot).row;
    } else if (current_direction == "west") {
      --positions->operator[](robot).col;
    }
  } 
  else if (command == "forward_2") {
    if (current_direction == "north") {
      positions->operator[](robot).row -= 2;
    } else if (current_direction == "east") {
      positions->operator[](robot).col -= 2;
    } else if (current_direction == "south") {
      positions->operator[](robot).row += 2;
    } else if (current_direction == "west") {
      positions->operator[](robot).col += 2;
    }
  } 
  else if (command == "forward_3") {
    if (current_direction == "north") {
      positions->operator[](robot).row -= 3;
    } else if (current_direction == "east") {
      positions->operator[](robot).col -= 3;
    } else if (current_direction == "south") {
      positions->operator[](robot).row += 3;
    } else if (current_direction == "west") {
      positions->operator[](robot).col += 3;
    }
  }
  else if (command == "forward_1") {
    if (current_direction == "north") {
      positions->operator[](robot).row -= 1;
    } else if (current_direction == "east") {
      positions->operator[](robot).col -= 1;
    } else if (current_direction == "south") {
      positions->operator[](robot).row += 1;
    } else if (current_direction == "west") {
      positions->operator[](robot).col += 1;
    }
  }
  else if (command == "do_nothing") {}
  else {
    die("this command is not valid"); 
  }

  if (this->moves->operator[](robot).size() > 0) {
    this->moves->operator[](robot).pop_back();
  }
}

void Board::printSolutions() const {
  for (std::map<const Robot*, std::vector<std::string> >::const_iterator itr = this->moves->begin();
      itr != this->moves->end(); itr++) {
    std::cout << "Robot " << itr->first->getSymbol() << ": " << itr->second[0];
    for (unsigned int i = 1; i < itr->second.size(); i++) {
      std::cout << ", " << itr->second[i];
    }
    std::cout << std::endl;
  }
}

int Board::checkNotMove(const std::string &str) {
  return (str == "rotate_right" || str == "rotate_left" ||
          str == "u_turn" || str == "do_nothing");
}

int Board::checkPush(const Robot *r1, const Robot *r2) {
  std::vector<std::string> moves1 = this->moves->operator[](r1);
  std::vector<std::string> moves2 = this->moves->operator[](r2);
  if (moves1.size() > 0) {
    if (this->checkNotMove(moves1.back())) {
      std::string direction = this->directions->operator[](r2);  
      if (direction == "north") {
        positions->operator[](r1).row += 1;
      } else if (direction == "east") {
        positions->operator[](r1).col += 1;
      } else if (direction == "south") {
        positions->operator[](r1).row -= 1;
      } else if (direction == "west") {
        positions->operator[](r1).col -= 1;
      }
      return 1;
    } else if (this->checkNotMove(moves2.back())) {
      std::string direction = this->directions->operator[](r1);  
      if (direction == "north") {
        positions->operator[](r2).row += 1;
      } else if (direction == "east") {
        positions->operator[](r2).col += 1;
      } else if (direction == "south") {
        positions->operator[](r2).row -= 1;
      } else if (direction == "west") {
        positions->operator[](r2).col -= 1;
      }
      return 1;
    }
    else {
      return 0;
    }
  }
  return 0;
}
  
int Board::checkCollisions(int pushing) {
  std::vector<Position> locations;
  for (std::map<const Robot*, Position>::const_iterator itr = this->positions->begin();
      itr != this->positions->end(); itr++) {
    std::map<const Robot*, Position>::const_iterator temp = itr;
    for (std::map<const Robot*, Position>::const_iterator itr2 = ++temp;
        itr2 != this->positions->end(); itr2++) {
      if (itr->second == itr2->second) {
        if (pushing) {
          if (!checkPush(itr->first, itr2->first)) return 0;
        }
        else {
          return 0;
        }
      }
    }
    this->addPastLocations(itr->first, locations);
  }

  for (unsigned int i = 0; i < locations.size(); i++) {
    for (unsigned int j = i + 1; j < locations.size(); j++) {
      if (locations[i] == locations[j]) {
        return 0;
      }
    }
  }

  return 1;
}

void Board::addPastLocations(const Robot *robot, std::vector<Position> &locations) {
  std::vector<std::string> previous_moves = this->moves->operator[](robot);
  if (previous_moves.size() == 0) return;
  std::string command = previous_moves.back();
  std::string current_direction = directions->operator[](robot);
  Position pos = this->positions->operator[](robot);
  if (command == "forward_1" || command == "forward_2" || command == "forward_3") {
    if (current_direction == "north") {
      locations.push_back(Position(pos.col, pos.row - 1));
    } else if (current_direction == "east") {
      locations.push_back(Position(pos.col - 1, pos.row));
    } else if (current_direction == "south") {
      locations.push_back(Position(pos.col, pos.row + 1));
    } else if (current_direction == "west") {
      locations.push_back(Position(pos.col + 1, pos.row));
    }
  }
  if (command == "forward_2" || command == "forward_3") {
    if (current_direction == "north") {
      locations.push_back(Position(pos.col, pos.row - 2));
    } else if (current_direction == "east") {
      locations.push_back(Position(pos.col - 2, pos.row));
    } else if (current_direction == "south") {
      locations.push_back(Position(pos.col, pos.row + 2));
    } else if (current_direction == "west") {
      locations.push_back(Position(pos.col + 2, pos.row));
    }
  }
  if (command == "forward_3") {
    if (current_direction == "north") {
      locations.push_back(Position(pos.col, pos.row - 3));
    } else if (current_direction == "east") {
      locations.push_back(Position(pos.col - 3, pos.row));
    } else if (current_direction == "south") {
      locations.push_back(Position(pos.col, pos.row + 3));
    } else if (current_direction == "west") {
      locations.push_back(Position(pos.col + 3, pos.row));
    }
  }
  if (command == "backward_1") {
    if (current_direction == "north") {
      locations.push_back(Position(pos.col, pos.row + 1));
    } else if (current_direction == "east") {
      locations.push_back(Position(pos.col + 1, pos.row));
    } else if (current_direction == "south") {
      locations.push_back(Position(pos.col, pos.row - 1));
    } else if (current_direction == "west") {
      locations.push_back(Position(pos.col - 1, pos.row));
    }
  }
}
  
int Board::outOfBounds() {
  for (std::map<const Robot*, Position>::const_iterator itr = this->positions->begin();
      itr != this->positions->end(); itr++) {
    if (itr->second.row < 0 || itr->second.col < 0) {
      return 1;
    } else if (itr->second.row >= this->rows || itr->second.col >= this->cols) {
      return 1;
    }
  }
  return 0;
}

int Board::checkGoals() {
  for (std::map<const Robot*, Position>::const_iterator itr = this->positions->begin();
      itr != this->positions->end(); itr++) {
    if (itr->first->getGoalPosition() != itr->second) {
      return 0;
    }
    if (this->moves->operator[](itr->first).size() != this->steps) {
      return 0;
    }
  }
  return 1;
}

std::vector<std::string> Board::print() const {

  // Prepare the return value
  std::vector<std::string> answer(this->rows*4+1, std::string(this->cols*4+1,' '));

  // Draw the outer border
  for (int i = 0; i < this->rows*4+1; i++) {
    answer[i][0] = '|';
    answer[i][this->cols*4] = '|';
  }
  for (int i = 0; i < this->cols*4+1; i++) {
    answer[0][i] = '-';
    answer[this->rows*4][i] = '-';
  }
  
  // Put tick marks at the corners of all grid cells
  for (int i = 0; i < this->cols*4+1; i+=4) {
    for (int j = 0; j < this->rows*4+1; j+=4) {
      answer[j][i] = '+';
    }
  }

  // draw each robot
  for (std::map<const Robot*, Position>::iterator itr = this->positions->begin();
      itr != this->positions->end(); itr++) {
    // grab the current position & direction
    Position p = itr->second;
    std::string direction = this->directions->operator[](itr->first);
    // calcuate the coordinate (flip the vertical axis)
    int r = (this->rows-1-p.row)*4+2;
    int c = p.col*4+2;
    // verify that no other robot is here
    //assert (answer[r][c] == ' ');
    // draw the letter
    answer[r][c] = itr->first->getSymbol();
    // draw the robot direction indicator
    if (direction == "north") {
      answer[r-1][c] = '^';
    } else if (direction == "east") {
      answer[r][c+1] = '>';
    } else if (direction == "south") {
      answer[r+1][c] = 'v';
    } else {
      assert (direction == "west");
      answer[r][c-1] = '<';
    }
  }

  return answer;
}

void Board::printBoards() const {
  if (this->ascii_boards->size() > 0) {
    for (unsigned int i = 0; i < this->ascii_boards->operator[](0).size(); i++) {
      for (unsigned int j = 0; j < this->ascii_boards->size(); j++) {
        std::cout << this->ascii_boards->operator[](j)[i] << "   ";
      }
      std::cout << std::endl;
    }
  }
}

void Board::createBoards(int pushing) {
  // Return robots to the start
  for (std::map<const Robot*, Position>::iterator itr = this->positions->begin();
      itr != this->positions->end(); itr++) {
    itr->second = itr->first->getStartPosition();
  }

  for (std::map<const Robot*, std::string>::iterator itr = this->directions->begin();
      itr != this->directions->end(); itr++) {
    itr->second = itr->first->getStartDirection();
  }

  this->ascii_boards->push_back(this->print());
  for (unsigned int i = 0; i < this->steps; i++) {
    for(std::map<const Robot*, std::vector<std::string> >::iterator itr = this->moves->begin();
        itr != this->moves->end(); itr++) {
      this->move(itr->first, itr->second[i]);
    }
    this->checkCollisions(pushing);
    this->ascii_boards->push_back(this->print());
  }

  this->printBoards();
}
void Board::Init() {
  this->ascii_boards = new std::vector<std::vector<std::string> >;
  this->positions = new std::map<const Robot*, Position>;
  this->directions = new std::map<const Robot*, std::string>;
  this->moves = new std::map<const Robot*, std::vector<std::string> >;
}

void Board::Clone(const Board &board) {
  this->Init();

  for (unsigned int i = 0; i < board.ascii_boards->size(); i++) {
    this->ascii_boards->push_back(board.ascii_boards->operator[](i));
  }

  this->positions->insert(board.positions->begin(), board.positions->end());
  this->directions->insert(board.directions->begin(), board.directions->end());
  this->moves->insert(board.moves->begin(), board.moves->end());

  this->verbose = board.verbose;
  this->rows = board.rows;
  this->cols = board.cols;
  this->steps = board.steps;
}

bool Board::operator==(const Board &b) const {
  return (*(this->positions) == *(b.positions) &&
      *(this->directions) == *(b.directions) &&
      *(this->moves) == *(b.moves) &&
      this->rows == b.rows &&
      this->cols == b.cols &&
      this->steps == b.steps);
}
