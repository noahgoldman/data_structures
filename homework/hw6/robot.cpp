#include <iostream>
#include <cassert>
#include <cstdlib>

#include "robot.h"
#include "board.h"


// CONSTRUCTOR
Robot::Robot(char symbol_, const Position &start_position_,
              const std::string &start_direction_, const Position &goal_position_,
              std::vector<std::string> *commands_) :
  symbol(symbol_), start_position(start_position_),
  start_direction(start_direction_), goal_position(goal_position_),
  commands(commands_) {}

void Robot::Clone(const Robot& old) {
  this->commands = new std::vector<std::string>;
  for (std::vector<std::string>::const_iterator itr = old.commands->begin();
      itr != old.commands->end(); itr++) {
    this->commands->push_back(*itr);
  }

  this->symbol = old.symbol;
  this->start_position = old.start_position;
  this->start_direction = old.start_direction;
  this->goal_position = old.goal_position;
}

void Robot::Delete() {
  delete this->commands;
}
