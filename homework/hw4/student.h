#ifndef _STUDENT_H_
#define _STUDENT_H_

class Student {
  public:
    explicit Student(const std::string &name);
    void AddSchool(const std::string &school_name);
    void RemoveSchool(const std::string &school_name);
    const bool IsOfferTentativelyAccepted(const std::string &school_name);
    void PrepareToReceiveOffers();

    void PrintStudentDecision(std::ofstream &file) const;
    void printPreferenceList(std::ostream &file) const;

    const std::string GetName() const { return this->name; };
    inline const int HasOffer() const { return this->decision; };
    inline const std::string GetBestOffer() const { return this->offer; };

  private:
    std::string name;
    std::list<std::string> *schools;
    std::string offer;
    int decision;

};

#endif
