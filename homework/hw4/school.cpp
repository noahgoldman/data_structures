#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include "school.h"
#include "student.h"
#include "debug.h"

// Sort students alphanumerically
bool alpha_by_student_name(Student s1, Student s2) {
  return (s1.GetName() < s2.GetName());
}

// Sort strings alphanumerically
bool alphanumeric(std::string s1, std::string s2) {
  return (s1 < s2);
}

// Constructor to init member variables
School::School(const std::string &name, const int slots)
  : name(name),
  slots(slots) {
  this->students = new std::list<std::string>;  
  this->accepted = new std::list<std::string>;
}

// Add a student to the school's preference list
void School::AddStudent(const std::string &student_name) {
  for (std::list<std::string>::iterator itr = this->students->begin();
      itr != this->students->end(); itr++) {
    if (*itr == student_name) {
      std::cerr << "WARNING: could not add " << student_name 
        << " to the list because there are already on it" << std::endl;
      return;
    }
  }
  this->students->push_back(student_name);
}

// Add a student, inserting before the one that is listed
void School::AddStudent(const std::string &new_student,
                        const std::string &old_student) {

  int found = 0;
  for (std::list<std::string>::iterator itr = this->students->begin();
      itr != this->students->end(); itr++) {
    if (*itr == new_student) {
      warn("Could not add a duplicate student");
      return;
    }
    else if (*itr == old_student) {
      this->students->insert(itr, new_student);
      found = 1;
      return;
    }
  }

  if (!found) {
    std::cerr << "WARNING: could not add " << new_student << " to the list because "
      << old_student << " is not in the list" << std::endl;
  }
}

// Output the next student the school wants
const std::string School::MakeNextOffer() const {
  if (this->students->size() > 0) {
    std::string student = this->students->front();
    this->students->pop_front();
    return student;
  } else {
    return "";
  }
}

// Revoke a students acceptance from the school
void School::StudentDeclinesTentativeAcceptance(const std::string &student) {
  for (std::list<std::string>::iterator itr = this->accepted->begin();
      itr != this->accepted->end(); itr++) {
    if (*itr == student) {
      this->accepted->erase(itr);
      return;
    }
  }
}

// Add a student to the schools accepted list
void School::StudentTentativelyAcceptsOffer(const std::string &student) {
  this->accepted->push_back(student);
}

// Output the students who are in the accepted list
void School::PrintSchoolEnrollment(std::ofstream &file) {
  file << "student(s) who will be attending " << this->name << ":" << std::endl;
  this->accepted->sort(alphanumeric);
  for (std::list<std::string>::iterator itr = this->accepted->begin();
      itr != this->accepted->end(); itr++) {
    file << "  "  << *itr << std::endl;
  }

  int difference = this->slots - this->accepted->size();
  if (difference > 0) {
    file << "  [" << difference << " remaining slot(s) in enrollment]" << std::endl;
  }
}

// This function is not used
void School::PrepareToMakeOffers() {
}

// Print the school's preference list
void School::printPreferenceList(std::ostream &file) const {
  file << this->name << " preference list:" << std::endl;
  int count = 1;
  for (std::list<std::string>::iterator itr = this->students->begin();
      itr != this->students->end(); itr++, count++) {
    if (count > 9) {
      file << " ";
    } else {
      file << "  ";
    }
    file << count << ". " << *itr << std::endl;
  }
  file << std::endl;
}
