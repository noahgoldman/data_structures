HOMEWORK 4: PREFERENCE LISTS


NAME: Noah Goldman 


TIME SPENT ON THIS ASSIGNMENT: 6 hours


COLLABORATORS: 
You must do this assignment on your own, as described in the DS
Academic Integrity Policy.  If you did discuss the problem or error
messages, etc. with anyone, please list their names here.



ORDER NOTATION:


add_school: O(m + r)

insert_student_into_school_preference_list: O(m + r)

print_school_preferences: O(m + r)

add_student: O(n + p)

remove_school_from_student_preference_list: O(n + p)

print_student_preferences: O(n + p)

perform_matching: O(m^2 * n)

print_school_enrollments: O(s)

print_student_decisions: O(1)



MISC. COMMENTS TO GRADER:  
Optional, please be concise!
