#include <stdio.h>
#include "debug.h"
#include "school.h"

int tests_run = 0;

School s("arg", 10);
s.AddStudent("noah");
s.AddStudent("bob");
s.AddStudent("jen");
s.AddStudent("arthur");
s.AddStudent("chad");

static const char* test_slots() {
  mu_assert("Slots not correct", s.MaxAcceptedStudents() == 10);
  mu_assert("Accepted not correct", s.NumAcceptedStudents() == 0);
  return 0;
}

static const char* all_tests() {
  mu_run_test(test_slots);
  return 0;
}

int main(int argc, char **argv) {
   const char *result = all_tests();
   if (result != 0) {
       printf("%s\n", result);
   }
   else {
       printf("ALL TESTS PASSED\n");
   }
   printf("Tests run: %d\n", tests_run);

   return result != 0;
}
