#ifndef _SCHOOL_H_
#define _SCHOOL_H_

#include <list>
#include <string>
#include <fstream>
#include "student.h"

bool alpha_by_student_name(Student s1, Student s2);

class School {
  public:
    School(const std::string &name, const int slots);
    void AddStudent(const std::string &student_name);
    void AddStudent(const std::string &new_student,
                     const std::string &old_student);
    const std::string MakeNextOffer() const;
    void StudentDeclinesTentativeAcceptance(const std::string &student);
    void StudentTentativelyAcceptsOffer(const std::string &student);
    void PrintSchoolEnrollment(std::ofstream &file);
    void PrepareToMakeOffers();
    void printPreferenceList(std::ostream &file) const;

    inline const std::string GetName() const { return this->name; };
    inline const int NumAcceptedStudents() const { return this->accepted->size(); };
    inline const int MaxAcceptedStudents() const { return this->slots; };
  
  private:
    std::string name;
    int slots;
    std::list<std::string> *students;
    std::list<std::string> *accepted;

};

#endif
