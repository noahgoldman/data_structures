#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include "student.h"
#include "debug.h"

Student::Student(const std::string &name) : name(name) {
  this->schools = new std::list<std::string>;
  this->decision = 0;
  this->offer = " ";
}

// Add a school to the students preference list
void Student::AddSchool(const std::string &school_name) {
  for (std::list<std::string>::iterator itr = this->schools->begin();
      itr != this->schools->end(); itr++) {
    if (*itr == school_name) {
      std::cerr << "WARNING: could not add " << school_name << " to " << this->name
        << "'s preference list because it already exists" << std::endl;
      return;
    }
  }
  this->schools->push_back(school_name);
}

// Remove a school from a students preference list
void Student::RemoveSchool(const std::string &school_name) {
  for (std::list<std::string>::iterator itr = this->schools->begin();
      itr != this->schools->end(); itr++) {
    if (*itr == school_name) {
      this->schools->erase(itr);
      return;
    }
  }

  std::cerr << "WARNING: could not remove " << school_name 
    << " because it isn't in the students preference list" << std::endl;
}

// Check if a student will accept the offer from the school
const bool Student::IsOfferTentativelyAccepted(const std::string &school_name) {
  for (std::list<std::string>::iterator itr = this->schools->begin();
      itr != this->schools->end(); itr++) {
    if (*itr == this->offer) {
      return false;
    }
    else if (*itr == school_name) {
      this->offer = school_name;
      this->decision = 1;
      return true;
    }
  }
  return false;
}

// Print the school the student has decided to attend
void Student::PrintStudentDecision(std::ofstream &file) const {
  if (this->decision) {
    file << this->name << " will be attending " << this->offer << std::endl; 
  } else {
    file << this->name << " has not received an acceptable offer" << std::endl;
  }
}

// This function is not used
void Student::PrepareToReceiveOffers() {
}

// Print a students preference list
void Student::printPreferenceList(std::ostream &file) const {
  file << this->name << " preference list:" << std::endl;
  int count = 1;
  for (std::list<std::string>::iterator itr = this->schools->begin();
      itr != this->schools->end(); itr++, count++) {
    file << "  " << count << ". " << *itr << std::endl;
  }
  file << std::endl;
}
