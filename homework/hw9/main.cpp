#include <math.h>
#include <limits.h>
#include <stdio.h>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <vector>
#include "image.h"

// ===================================================================================================
// ===================================================================================================

typedef std::pair<int, int> Coord;

double round(double d) {
  return floor(d + 0.5);
}

void get_factors(int n, std::vector<int> factors) {
  for (int i = 2; i < sqrt(n); i++) {
    if (n % i == 0) {
      factors.push_back(i);
    }
  }
}

int share_factors(int n, int d) {
  std::vector<int> n_factors, d_factors;
  get_factors(n, n_factors);
  get_factors(d, d_factors);
  unsigned int i = 0, j = 0;
  while (i < n_factors.size() && j < d_factors.size()) {
    if (n_factors[i] == d_factors[j]) {
      return 1;
    } else if (n_factors[i] > d_factors[j]) {
      j++;
    } else { // n_factors[i] < d_factors[j]
      i++;
    }
  }
  return 0;
}

unsigned int create_occupancy(const Image<Color> &image,
                              Image<bool> &occupancy) {
  unsigned int count = 0;
  for (int i = 0; i < image.Width(); i++) {
    for (int j = 0; j < image.Height(); j++) {
      int res;
      Color color = image.GetPixel(i, j);
      if (!color.isWhite()) {
        res = 1;
        ++count;
      } else {
        res = 0;
      }
      occupancy.SetPixel(i, j, res);
    }
  }
  return count;
}

std::pair<Coord, std::vector<Coord> > pop_next_offset(std::map<Coord,
                                    std::vector<Coord> > &offset_counts) {
  std::map<Coord, std::vector<Coord> >::iterator itr, max;
  unsigned int count_max = 0;
  for (itr = offset_counts.begin(); itr != offset_counts.end(); itr++) {
    if (itr->second.size() > count_max) {
      max = itr;
      count_max = itr->second.size();
    }
  }
  
  std::pair<Coord, std::vector<Coord> > res = std::make_pair(max->first,
      max->second); 
  offset_counts.erase(max);
  return res;
}

int check_offset_value(int dx, int dy, int hash_data_size, const std::vector<Coord> &colors,
                      Image<Color> &hash_data) {
  std::vector<Coord> coords;
  for (unsigned int k = 0; k < colors.size(); k++) {
    unsigned int hash_x = (colors[k].first + dx) % hash_data_size;
    unsigned int hash_y = (colors[k].second + dy) % hash_data_size;
    Color pixel = hash_data.GetPixel(hash_x, hash_y);
    if (!pixel.isWhite()) {
      return 0;
    }
    if (colors.size() != 1) {
      std::pair<int, int> coord = std::make_pair(hash_x, hash_y);
      for (unsigned int i = 0; i < coords.size(); i++) {
        if (coords[i].first == coord.first && coords[i].second == coord.second) {
          return 0;
        }
      }
      coords.push_back(coord);
    }
  }
  return 1;
}

void set_hash_data(int dx, int dy, std::vector<Coord> &colors,
                    Image<Color> &hash_data, const Image<Color> &image) {
  unsigned int hash_data_size = hash_data.Width();
  for (unsigned int i = 0; i < colors.size(); i++) {
    Color color = image.GetPixel(colors[i].first, colors[i].second);
    unsigned int hash_x = (colors[i].first + dx) % hash_data_size;
    unsigned int hash_y = (colors[i].second + dy) % hash_data_size;

    assert(hash_data.GetPixel(hash_x, hash_y).isWhite());

    hash_data.SetPixel(hash_x, hash_y, color);
  }
}

int try_write_offset(const Image<Color> &input, int hash_data_size,
                      std::map<Coord, std::vector<Coord> > &offset_counts,
                      Image<Color> &hash_data, Image<Offset> &offset) {
  std::pair<Coord, std::vector<Coord> > res = pop_next_offset(offset_counts);
  Coord offset_coords = res.first; 
  for (int dx = 0; dx < 16; dx++) {
    for (int dy = 0; dy < 16; dy++) {
      if (check_offset_value(dx, dy, hash_data_size, res.second, hash_data)) {
        Offset offset_value(dx, dy);
        offset.SetPixel(offset_coords.first, offset_coords.second, offset_value);
        //print_coords(res.second);
        set_hash_data(dx, dy, res.second, hash_data, input);
        return 1;
      }
    }
  }
  return 0;
}

int search_offset(const Image<Color> &input, int hash_data_size,
                Image<Color> &hash_data, Image<Offset> &offset,
                const Image<bool> &occupancy) {
  int offset_size = offset.Width();
  std::map<Coord, std::vector<Coord> > offset_counts;
  for (int i = 0; i < occupancy.Width(); i++) {
    for (int j = 0; j < occupancy.Height(); j++) {
      if (occupancy.GetPixel(i, j)) {
        offset_counts[std::make_pair(i % offset_size, j % offset_size)].push_back(
            std::make_pair(i, j));
      }
    }
  }

  while (offset_counts.size() > 0) {
    if (!try_write_offset(input, hash_data_size, offset_counts, hash_data, offset)) return 0;
  }
  return 1;
}
      

void Compress(const Image<Color> &input, 
              Image<bool> &occupancy, Image<Color> &hash_data,
              Image<Offset> &offset) {
  occupancy.Allocate(input.Width(), input.Height());
  unsigned int non_white = create_occupancy(input, occupancy);

  int hash_data_size = round(sqrt(1.01 * non_white));
  int offset_size = ceil(sqrt(non_white / 4));

  Color white;
  hash_data.SetAllPixels(white);


  int perfect = 0;
  while (!perfect) {
    while(share_factors(offset_size, hash_data_size)) {
      if(++offset_size % 5 == 0) ++hash_data_size;
    }
    offset.Allocate(offset_size, offset_size);
    hash_data.Allocate(hash_data_size, hash_data_size);
    hash_data.SetAllPixels(white);

    perfect = search_offset(input, hash_data_size, hash_data, offset, occupancy);
    if(++offset_size % 5 == 0) ++hash_data_size;
  }
}


void UnCompress(const Image<bool> &occupancy, const Image<Color> &hash_data,
                const Image<Offset> &offset, Image<Color> &output) {
  output.Allocate(occupancy.Width(), occupancy.Height());
  for (int i = 0; i < occupancy.Width(); i++) {
    for (int j = 0; j < occupancy.Height(); j++) {
      if (occupancy.GetPixel(i, j)) {
        int size = offset.Width();
        Offset off_value = offset.GetPixel(i % size, j % size);

        int hash_size = hash_data.Width();
        Color color = hash_data.GetPixel((i+off_value.dx) % hash_size,
          (j+off_value.dy) % hash_size);
        
        output.SetPixel(i, j, color);
      }
    }
  }
}


// ===================================================================================================
// ===================================================================================================

void Compare(const Image<Color> &input1, const Image<Color> &input2, Image<bool> &output) {

  // confirm that the files are the same size
  if (input1.Width() != input2.Width() ||
      input1.Height() != input2.Height()) {
    std::cerr << "Error: can't compare images of different dimensions: " 
         << input1.Width() << "x" << input1.Height() << " vs " 
         << input2.Width() << "x" << input2.Height() << std::endl;
  } else {
    
    // confirm that the files are the same size
    output.Allocate(input1.Width(),input1.Height());
    int count = 0;
    for (int i = 0; i < input1.Width(); i++) {
      for (int j = 0; j < input1.Height(); j++) {
        Color c1 = input1.GetPixel(i,j);
        Color c2 = input2.GetPixel(i,j);
        if (c1.r == c2.r && c1.g == c2.g && c1.b == c2.b)
          output.SetPixel(i,j,true);
        else {
          count++;
          output.SetPixel(i,j,false);
        }
      }      
    }     

    // confirm that the files are the same size
    if (count == 0) {
      std::cout << "The images are identical." << std::endl;
    } else {
      std::cout << "The images differ at " << count << " pixel(s)." << std::endl;
    }
  }
}

// ===================================================================================================
// ===================================================================================================


// to allow visualization of the custom offset image format
void ConvertOffsetToColor(const Image<Offset> &input, Image<Color> &output) {
  // prepare the output image to be the same size as the input image
  output.Allocate(input.Width(),input.Height());
  for (int i = 0; i < output.Width(); i++) {
    for (int j = 0; j < output.Height(); j++) {
      // grab the offset value for this pixel in the image
      Offset off = input.GetPixel(i,j);
      // set the pixel in the output image
      int dx = off.dx;
      int dy = off.dy;
      assert (dx >= 0 && dx <= 15);
      assert (dy >= 0 && dy <= 15);
      // to make a pretty image with purple, cyan, blue, & white pixels:
      int red = dx * 16;
      int green = dy *= 16;
      int blue = 255;
      assert (red >= 0 && red <= 255);
      assert (green >= 0 && green <= 255);
      output.SetPixel(i,j,Color(red,green,blue));
    }
  }
}


// ===================================================================================================
// ===================================================================================================

void usage(char* executable) {
  std::cerr << "Usage:  4 options" << std::endl;
  std::cerr << "  1)  " << executable << " compress input.ppm occupancy.pbm data.ppm offset.offset" << std::endl;
  std::cerr << "  2)  " << executable << " uncompress occupancy.pbm data.ppm offset.offset output.ppm" << std::endl;
  std::cerr << "  3)  " << executable << " compare input1.ppm input2.ppm output.pbm" << std::endl;
  std::cerr << "  4)  " << executable << " visualize_offset input.offset output.ppm" << std::endl;
}


// ===================================================================================================
// ===================================================================================================

int main(int argc, char* argv[]) {
  if (argc < 2) { usage(argv[1]); exit(1); }

  if (argv[1] == std::string("compress")) {
    if (argc != 6) { usage(argv[1]); exit(1); }
    // the original image:
    Image<Color> input;
    // 3 files form the compressed representation:
    Image<bool> occupancy;
    Image<Color> hash_data;
    Image<Offset> offset;
    input.Load(argv[2]);
    Compress(input,occupancy,hash_data,offset);
    // save the compressed representation
    occupancy.Save(argv[3]);
    hash_data.Save(argv[4]);
    offset.Save(argv[5]);

  } else if (argv[1] == std::string("uncompress")) {
    if (argc != 6) { usage(argv[1]); exit(1); }
    // the compressed representation:
    Image<bool> occupancy;
    Image<Color> hash_data;
    Image<Offset> offset;
    occupancy.Load(argv[2]);
    hash_data.Load(argv[3]);
    offset.Load(argv[4]);
    // the reconstructed image
    Image<Color> output;
    UnCompress(occupancy,hash_data,offset,output);
    // save the reconstruction
    output.Save(argv[5]);
  
  } else if (argv[1] == std::string("compare")) {
    if (argc != 5) { usage(argv[1]); exit(1); }
    // the original images
    Image<Color> input1;
    Image<Color> input2;    
    input1.Load(argv[2]);
    input2.Load(argv[3]);
    // the difference image
    Image<bool> output;
    Compare(input1,input2,output);
    // save the difference
    output.Save(argv[4]);

  } else if (argv[1] == std::string("visualize_offset")) {
    if (argc != 4) { usage(argv[1]); exit(1); }
    // the 8-bit offset image (custom format)
    Image<Offset> input;
    input.Load(argv[2]);
    // a 24-bit color version of the image
    Image<Color> output;
    ConvertOffsetToColor(input,output);
    output.Save(argv[3]);

  } else {
    usage(argv[0]);
    exit(1);
  }
}
