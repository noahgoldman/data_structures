HOMEWORK 9: PERFECT HASHING


NAME: Noah Goldman


COLLABORATORS AND OTHER RESOURCES:


SUMMARY OF RESULTS: 
I was able to construct a perfect hash for all three of the test cases.  The smallest ratio I got was on the lightbulb test case, where the compression ratio was 0.15.


OFFSET SEARCH FOR EXTRA CREDIT:
I implemented my offset search function to check weather it can actually assign a particular offset based on the values in the hash data table.  If the method failed to find dx and dy values for an offset cell, the offset table size will increase by 1 and the program will try again.  Additionaly, every 5 times the offset table increases will result in an increase of 1 in the hash table size.

MISC. COMMENTS TO GRADER:  
