#include <algorithm>
#include "superhero.h"
#include "team.h"

bool isvowel(char c) {
  if (!isalpha(c)) return false;
  char c2 = tolower(c);
  return (c2 == 'a' || c2 == 'e' || c2 == 'i' || c2 == 'o' || c2 == 'u');
}

bool isconsonant(char c) {
  return (isalpha(c) && !isvowel(c));
}

std::string Team::getName() const {
  if (superheroes.size() == 0) 
    return std::string("");
  std::string answer;
  std::list<Superhero>::const_iterator itr;
  for (itr = superheroes.begin(); itr != superheroes.end(); itr++) {
    char first_consonant = ' ';
    char first_vowel = ' ';
    std::string true_identity = itr->getTrueIdentity();
    for (int j = 0; j < true_identity.size(); j++) {
      if (first_consonant == ' ' && isconsonant(true_identity[j]))
        first_consonant = tolower(true_identity[j]);
      if (first_vowel == ' ' && isvowel(true_identity[j]))
        first_vowel = tolower(true_identity[j]);
    }
    answer.push_back(first_consonant);
    answer.push_back(first_vowel);
  }

  answer[0] = toupper(answer[0]);
  return answer;
}

void Team::operator+=(const Superhero& sh) {
  this->superheroes.push_back(sh);
}

void Team::operator-=(const Superhero& sh) {
  for (std::list<Superhero>::iterator itr = this->superheroes.begin();
      itr != this->superheroes.end(); itr++) {
    if (itr->getName() == sh.getName()) {
      this->superheroes.erase(itr);
      return;
    }
  }
}

void Team::operator()() {
  std::cout << "\nTeam " << this->getName() << " turns against each other!" << std::endl;
  for (std::list<Superhero>::iterator itr = this->superheroes.begin();
      itr != this->superheroes.end(); itr++) {
    for (std::list<Superhero>::iterator itr2 = this->superheroes.begin();
        itr2 != this->superheroes.end(); itr2++) {
      if (itr2 != itr) {
        if (*itr > *itr2) {
          std::cout << itr->getName() << " defeats the " << itr2->getName() << " with the "
            << itr->getPower() << " power." << std::endl;
          itr2 = this->superheroes.erase(itr2);
        } else { 
          std::cout << itr2->getName() << " defeats the " << itr->getName() << " with the "
          << itr2->getPower() << " power." << std::endl;
          itr = this->superheroes.erase(itr);
        }
      }
    }
  }

  std::cout << std::endl;
  for (std::list<Superhero>::const_iterator itr = this->superheroes.begin();
      itr != this->superheroes.end(); itr++) {
    std::cout << itr->getName() << " survived!" << std::endl;
  }
}

Team operator+(const Team &a, const Team &b) {
  Team team;
  std::list<Superhero>::const_iterator itr;
  for (itr = a.superheroes.begin(); itr != a.superheroes.end(); itr++) {
    team += *itr;
  }
  for (itr = b.superheroes.begin(); itr != b.superheroes.end(); itr++) {
    team += *itr;
  }
  return team;
}

Team operator+(const Team &a, const Superhero &b) {
  Team team;
  std::list<Superhero>::const_iterator itr;
  for (itr = a.superheroes.begin(); itr != a.superheroes.end(); itr++) {
    team += *itr;
  }
  team += b; 
  return team;
}

Team operator+(const Superhero &a, const Superhero &b) {
  Team team;
  team += a;
  team += b;
  return team;
}
