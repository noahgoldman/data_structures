#ifndef SUPERHERO_H_
#define SUPERHERO_H_

#include <string>

class Team;

class Superhero {
  public:
    Superhero(const std::string& name, const std::string& identity,
              const std::string& power);

    inline const std::string& getName() const { return name; }
    inline const std::string& getPower() const { return power; }
    inline int isGood() const { return this->good; }

    inline Superhero& operator-() { this->good = !this->good; }

    bool operator==(const std::string& str);
    bool operator!=(const std::string& str);

    bool operator>(const Superhero& sh1); 

    friend class Team;

  private:
    const std::string name;
    const std::string identity;
    const std::string power;
    int good;

    inline const std::string& getTrueIdentity() const { return this->identity; }
};

std::ostream& operator<<(std::ostream& os, const Superhero& sh);

#endif // SUPERHERO_H_
