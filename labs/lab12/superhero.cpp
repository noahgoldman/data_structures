#include <fstream>
#include "superhero.h"

std::ostream& operator<<(std::ostream& os, const Superhero &sh) {
  os << sh.getName() << " has power " << sh.getPower() << std::endl;
}

Superhero::Superhero(const std::string& name, const std::string& identity,
                      const std::string& power) : name(name),
  identity(identity),
  power(power),
  good(1) {}


bool Superhero::operator==(const std::string& str) {
  return (str == this->identity);
}

bool Superhero::operator!=(const std::string& str) {
  return (str != this->identity);
}

bool Superhero::operator>(const Superhero& sh) {
  std::string p = sh.power;
  if (this->power == "Fire") {
    if (p == "Flexible" || p == "Wood") return 1;
  } else if (this->power == "Flying") {
    if (p == "Fire" || p == "Water" || p == "Speed") return 1;
  } else if (this->power == "Invisible") {
    return 1;
  } else if (this->power == "Laser") {
    if (p == "Flexible" || p == "Flying" || p == "Fire" || p == "Water") return 1;
  } else if (this->power == "Wood") {
    if (p == "Water" || p == "Flexible" || p == "Laser" || p == "Speed") return 1;
  } else if (this->power == "Water") {
    if (p == "Fire" || p == "Flexible") return 1;
  } else if (this->power == "Speed") {
    if (p == "Laser" || p == "Fire" || p == "Wood" || p == "Flexible") return 1;
  }
  return 0;
}
