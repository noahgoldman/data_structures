#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

int get_paths(int x, int y) {
  if (x > 0 && y > 0) {
    return get_paths(x - 1, y) + get_paths(x, y - 1);
  } else if (x > 0 && y < 1) {
    return get_paths(x - 1, 0);
  } else if (y > 0 && x < 1) {
    return get_paths(0, y - 1);
  }
  return 1;
}

int main(int argc, char *argv[]) {
  assert(argc == 3);   
  int x = atoi(argv[1]);
  int y = atoi(argv[2]);

  int res = get_paths(x, y);
  printf("Number of paths: %d\n", res);

  return 0;
}
