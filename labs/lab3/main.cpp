#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <cassert>
#include <iostream>

void print_stack(uintptr_t* bottom, uintptr_t* top) {

  // double check that my uintptr_t is actually the size of a pointer
  assert (sizeof(uintptr_t) == sizeof(void*));

  // double check that the start and end address are aligned 
  assert ((((uintptr_t)bottom) & (sizeof(uintptr_t)-1)) == 0);
  assert ((((uintptr_t)top) & (sizeof(uintptr_t)-1)) == 0);

  // we assume that the top address is greater than the value on
  // bottom address.  
  assert (bottom <= top);

  std::cout << "-----------------------------------------" << std::endl;

  for (uintptr_t* address = top; address >= bottom; address--) {
    // this location might store a value (we assume all values are integers)
    uintptr_t value = *address;

    // or it might store an address (so we cast it to a pointer)
    uintptr_t* if_address = (uintptr_t*)value;

    std::cout << "location: " << address;

    // if it's a value
    if (value < 1000) 
      std::cout << "  VALUE:   " << value << std::endl;

    // if it's an address
    else if (if_address >= bottom-1000 && if_address <= top+1000)
      std::cout << "  POINTER: " << if_address << std::endl;

    // otherwise it's probably garbage
    else 
      std::cout << "  garbage? " << std::endl;
  }
  std::cout << "-----------------------------------------" << std::endl;
}

void compute_square(uintptr_t *a, uintptr_t *b, int n) {
  int i;
  for(i = 0; i < n; i++) {
    *(b + i) = pow(*(a + i), 2);
  }
}

void print_array(uintptr_t arr[], int n) {
  int i;
  for (i = 0; i < n; i++) {
    printf("%d ", *(arr + i));
  }
  printf("\n");
}

int main(int argc, char *argv[]) {
  uintptr_t test1[] = {4, 6, 2, 19, 2};
  //uintptr_t test2[] = {19, 24, 62, 92};
  //uintptr_t test3[] = {12, 54, 77, 82, 35};

  uintptr_t o1[5];//, o2[4], o3[6];

  print_stack(test1, o1+8);
  compute_square(test1, o1, 9);
  print_stack(test1, o1+8);

  print_array(o1, 5);
  //print_array(o2, 4);
  //print_array(o3, 6);

  return 0;
}
