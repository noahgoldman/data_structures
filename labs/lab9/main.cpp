#include <assert.h>
#include <fstream>
#include <algorithm>
#include <map>
#include <vector>

int main(int argc, char **argv) {
  assert(argc == 2);
  char *filename = argv[1];
  
  std::ifstream file(filename);
  std::map<int, int> count;
  
  int integer;
  while (file >> integer) {
    std::pair<std::map<int, int>::iterator, bool> res = 
      count.insert(std::make_pair(integer, 1));
    if (!res.second) {
      std::map<int, int>::iterator itr = count.find(integer);
      itr->second++;
    }
  }

  std::vector<int> modes;
  int max = 0;
  for (std::map<int, int>::const_iterator itr = count.begin();
      itr != count.end(); itr++) {
    max = std::max(max, itr->second);
  }

  for (std::map<int, int>::const_iterator itr = count.begin();
      itr != count.end(); itr++) {
    if (itr->second == max) {
      modes.push_back(itr->first);
    }
  }

  printf("Found %d modes:\n", modes.size());
  printf("%d", modes[0]);
  for (int i = 1; i < modes.size(); i++) {
    printf(", %d", modes[i]);
  }
  printf("\n");

  return 0;
}
