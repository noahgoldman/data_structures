#!/usr/bin/env python

import requests
import re
import pickle
from bs4 import BeautifulSoup
from sys import argv

def pull_colors():
    f = open('colors.txt', 'w')

    colors = []
    url = "http://en.wikipedia.org/wiki/List_of_colors"
    r = requests.get(url)
    soup = BeautifulSoup(r.text)

    table = soup.find('table', class_ = 'wikitable')
    for link in table.find_all('a'):
        output = slugify(link.get_text())
        colors.append(output)

    pickle.dump(colors, f)
    f.close()


def slugify(string):
    string = string.encode('ascii', 'ignore').lower()
    string = re.sub(r'[^a-z0-9]+', '_', string).strip('_')
    string = re.sub(r'[-]+', '_', string)
    return string

def find_post(base_url):
    f = open('colors.txt', 'r')
    colors = pickle.load(f)

    for color in colors:
        url = base_url + color + '.pdf'
        res = requests.get(base_url + color + '.pdf')
        if res.status_code == 200:
            print("The url for the lab is:")
            print(url)
            f.close()
            return 0

    f.close()
    print("No url was found")


if __name__ == '__main__':
    pull_colors()
    find_post(argv[1])
