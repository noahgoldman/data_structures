#include <stdio.h>
#include <math.h>
#include <iostream>

float average(float count, float values[]) {
  float sum = 0;
  int i;
  for (i = 0; i < count; i++) {
    sum += values[i];
  }

  float average = sum / count;
  return average;
}

float find_closest(float count, float values[], float average) {
  int closest = 0;
  float closest_difference = 1000;
  int i;
  for (i = 0; i < count; i++) {
    float difference = fabs(average - values[i]);
    if (difference < closest_difference) {
      closest = i;
      closest_difference = difference;
    }
  }

  return values[closest];
}

int main(int argc, char *argv[]) {
  int data_count;

  printf("Enter the number of data values ");
  scanf("%d", &data_count);
  float data[data_count];

  int i;
  for (i = 0; i < data_count; i++) {

    printf("Enter a data value ");
    scanf("%e", &data[i]);
  }

  float data_average = average(data_count, data);
  std::cout << "The average is " << data_average << std::endl;

  float closest = find_closest(data_count, data, data_average);
  std::cout << "The closest value to the average is " << closest << std::endl;

  return 0;
}
