#include "time.h"
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>

Time::Time() {
  this->hour = 0;
  this->minute = 0;
  this->second = 0;
}

Time::Time(int hour, int minute, int second) {
  this->hour = hour;
  this->minute = minute;
  this->second = second;
}

int Time::getHour() const {
  return this->hour;
}

int Time::getMinute() const {
  return this->minute;
}

int Time::getSecond() const {
  return this->second;
}

void Time::printTime() const {
  std::cout << this->prependZero(this->hour) << ":" << this->prependZero(this->minute) << ":" << this->prependZero(this->second) << std::endl;
}

void Time::setHour(int hour) {
  this->hour = hour;
}

void Time::setMinute(int minute) {
  this->minute = minute;
}

void Time::setSecond(int second) {
  this->second = second;
}

std::string Time::prependZero(int time) const {
  std::string output;
  std::stringstream out;
  out << time;
  output = out.str();
  if (time < 10) {
    output = "0" + output; 
  }

  return output.c_str();
}

void Time::printAmPm() {
  const char *am_pm;
  int corrected_hour;
  if (this->hour > 12) {
    am_pm = "pm";
    corrected_hour = this->hour - 12; 
  }
  else {
    am_pm = "am";
    corrected_hour = this->hour;
  }
  
  std::cout << this->prependZero(corrected_hour) << ":" << this->prependZero(this->minute) << ":" << this->prependZero(this->second) << std::endl;
}
