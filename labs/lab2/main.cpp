#include "time.h"
#include <iostream>
#include <assert.h>
#include <algorithm>
#include <vector>

int compareInts(int int1, int int2) {
  if (int1 == int2) return 2;
  else if (int1 < int2) return 1;
  else if (int1 > int2) return 0;
  assert(0);
}

bool isEarlierThen(const Time &t1, const Time &t2) {
  int hour_result = compareInts(t1.getHour(), t2.getHour());
  if (hour_result != 2) return hour_result;

  assert(hour_result != 2);
  
  int minute_result = compareInts(t1.getMinute(), t2.getMinute());
  if (minute_result != 2) return minute_result;

  int second_result = compareInts(t1.getSecond(), t2.getSecond());
  if (second_result != 2) return second_result;
}

int main(int argc, char *argv[]) {
  Time t1(5, 5, 5);
  Time t2(14, 30, 15);
  Time t3(20, 25, 59);
  Time t4(23, 40, 45);

  std::cout << "Millitary times:" << std::endl;
  t1.printTime();
  t2.printTime();

  std::cout << "AM/PM Times:" << std::endl;
  t1.printAmPm();
  t2.printAmPm();

  std::vector<Time> times;
  times.push_back(t3);
  times.push_back(t2);
  times.push_back(t4); 
  times.push_back(t1);

  sort(times.begin(), times.end(), isEarlierThen);

  std::cout << "Sorted Times:" << std::endl;

  int i;
  for (i = 0; i < times.size(); i++) {
    times[i].printAmPm();
  }

  return 0;
}  
