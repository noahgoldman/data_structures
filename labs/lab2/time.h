#include <string>

class Time {
  public:
    Time();
    Time(int hour, int minute, int second);
    
    int getHour() const;
    int getMinute() const;
    int getSecond() const;

    void printTime() const;

    void setHour(int hour);
    void setMinute(int minute);
    void setSecond(int second);

    std::string prependZero(int time) const;

    void printAmPm();
    
  private:
    int hour;
    int minute;
    int second;
};
