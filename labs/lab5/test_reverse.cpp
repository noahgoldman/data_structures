#include <stdio.h>
#include <iostream>
#include <vector>
#include "vec.h"

void reverse_l(Vec<int>& vec) {
  int max_index = vec.size() - 1;
  for (int i = 0; i < (vec.size() / 2); i++) {
    int temp = vec[i];
    vec[i] = vec[max_index - i];
    vec[max_index - i] = temp;
  }
}

template <class T> void reverse_2(std::vector<T>& vec) {
  typename std::vector<T>::iterator i;
  typename std::vector<T>::reverse_iterator ri;
  int c = 0;
  for (i = vec.begin(), ri = vec.rbegin(); c < (vec.size() / 2); ri++, i++, c++) {
    T temp = *i;
    *i = *ri;
    *ri = temp;
  }
}

void print(const Vec<int>& vec) {
  for (int i = 0; i < vec.size(); i++) {
    printf("%d ", vec[i]);
  }
  printf("\nSize: %d\n\n", vec.size());
}

template <class T> void print(const std::vector<T>& vec) {
  for (int i = 0; i < vec.size(); i++) {
    std::cout << vec[i] << " ";
  }
  std::cout << std::endl << "Size: " << vec.size() << std::endl << "\n";
}

int main() {
  Vec<int> v1;

  v1.push_back(1); 
  v1.push_back(3); 
  v1.push_back(5); 
  v1.push_back(8); 
  v1.push_back(2); 
  v1.push_back(3);
  v1.push_back(10);
  v1.push_back(27);
  v1.push_back(2); 

  print(v1);

  reverse_l(v1);

  print(v1);


  std::vector<int> v2;

  v2.push_back(1); 
  v2.push_back(3); 
  v2.push_back(5); 
  v2.push_back(8); 
  v2.push_back(2); 
  v2.push_back(3);
  v2.push_back(10);
  v2.push_back(27);
  v2.push_back(2); 

  print(v2);

  reverse_2(v2);

  print(v2);

  std::vector<char> v3;

  v3.push_back('a'); 
  v3.push_back('c'); 
  v3.push_back('f'); 
  v3.push_back('y'); 
  v3.push_back('b'); 
  v3.push_back('z');
  v3.push_back('x');

  print(v3);

  reverse_2(v3);

  print(v3);

  return 0;
}
