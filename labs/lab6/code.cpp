#include <iostream>

// a link in our linked list
class Node {
public:
  int value;
  Node* ptr;
};


// adds a new link with this value to the back of the list
void push_back(Node* &head, int v) {
  if (head == NULL) {
    head = new Node();
    head->value = v;
    head->ptr = NULL;
  } else {
    Node* temp;
    for (temp = head; temp->ptr != NULL; temp = temp->ptr) {}
    // temp is now pointing to the last node
    temp->ptr = new Node();
    temp->ptr->value = v;
    temp->ptr->ptr = NULL;  
  }
}

void push_front(Node* &head, int v) {
  if (head == NULL) {
    head = new Node();
    head->value = v;
    head->ptr = NULL;
  } else {
    Node *new_head = new Node();
    new_head->ptr = head;
    new_head->value = v;
    head = new_head;
  }
}

void clear(Node* head) {
  if (head != NULL) {
    Node *next = head->ptr;
    delete head;
    clear(next);
  }
}


// print out the contents of the list
void print(Node* &head) {
  Node* temp;
  std::cout << "contents: ";
  for (temp = head; temp != NULL; temp = temp->ptr) {
    std::cout << " " << temp->value;
  }  
  std::cout << "\n" << std::endl;
}

Node* remove(Node *head, const int search) {
  Node *original = head;
  if (head->value == search) {
    Node* next = head->ptr;
    delete head;
    return next;
  }
  while (head->ptr != NULL) {
    Node* next = head->ptr;
    if (head->ptr->value == search) {
      head->ptr = next->ptr;
      delete next;
      return original;
    }
    head = head->ptr;
  }
  std::cerr << "[ERROR] The element was not found in the list" << std::endl;
  return original;
}

int find_smallest(Node *head) {
  int smallest = head->value;
  while (head != NULL) {
    if (head->value < smallest) smallest = head->value;
    head = head->ptr;
  }
  return smallest;
}

/*
Node* sort(Node *head) {
  if (head->ptr != NULL) {
    int min = find_smallest(head);
    Node *new_head = remove(head, min);
    push_front(head, min);
    return sort(new_head);
  } else {
    return head;
  }
}
*/

Node* sort(Node *head) {
  if (head->ptr != NULL) {
    int min = find_smallest(head);
    head = remove(head, min);
    Node* front = new Node();
    front->value = min;
    front->ptr = sort(head);
    return front;
  } else {
    return head;
  }
}

int main() {

  // testing push_back and print
  Node* lst = NULL; 
  push_back(lst,10);
  push_back(lst,11);
  push_back(lst,13);
  push_back(lst,14);
  push_back(lst,15);
  push_back(lst,12);
  push_back(lst,16);
  push_back(lst,17);
  push_back(lst,18);
  print(lst);

  std::cout << "Remove an element in the middle" << std::endl;
  lst = remove(lst, 13);
  print(lst);

  std::cout << "Remove an element at the beginning" << std::endl;
  lst = remove(lst, 10);
  print(lst);

  std::cout << "Remove an element at the end" << std::endl;
  lst = remove(lst, 18);
  print(lst);

  std::cout << "Remove an element that doesn't exist" << std::endl;
  lst = remove(lst, 19);
  print(lst);


  std::cout << "Added a few more elements" << std::endl;
  push_back(lst, 2);
  push_back(lst, 9);
  push_front(lst, 5);
  push_front(lst, 123);
  print(lst);

  std::cout << "Sorted list:" << std::endl;
  lst = sort(lst);
  print(lst);

  clear(lst);
}
