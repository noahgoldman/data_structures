#include <stdio.h>
#include <list>
#include <iostream>

template <class T> void print(const std::list<T>& lst) {
  for (typename std::list<T>::const_iterator i = lst.begin(); i != lst.end(); i++) {
    std::cout << *i << " ";
  }
  std::cout << std::endl << "Size: " << lst.size() << "\n" << std::endl;
}

template <class T> void reverse_iterators(std::list<T> &lst) {
  typename std::list<T>::iterator forward;
  typename std::list<T>::reverse_iterator backward;
  unsigned int count = 0;
  for (forward = lst.begin(), backward = lst.rbegin();
      count < (lst.size() / 2);
      forward++, backward++, count++) {
    T temp = *forward;
    *forward = *backward;
    *backward = temp;
  }
}

template <class T> void reverse_stl(std::list<T> &lst) {
  typename std::list<T>::iterator i;
  for (i = lst.begin(); i != lst.end(); i++) {
    i = lst.insert(i, lst.back());
    lst.pop_back();
  }
}

int main(int argc, char *argv[]) {
  std::list<double> l1;

  l1.push_back(34);
  l1.push_back(21);
  l1.push_back(67);
  l1.push_back(78);
  l1.push_back(1);
  l1.push_back(71);
  l1.push_back(67);

  std::cout << "The unreversed list" << std::endl;
  print(l1);

  std::cout << "The reversed list with iterators and pointers:" << std::endl;
  reverse_iterators(l1);
  print(l1);


  std::cout << "Unreversing the list once again with STL functions:" 
    << std::endl;
  reverse_stl(l1);
  print(l1);
}
