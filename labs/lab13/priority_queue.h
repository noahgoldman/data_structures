#ifndef priority_queue_h_
#define priority_queue_h_

#include <assert.h>
#include <iostream>
#include <vector>
using namespace std;


template <class T>
class priority_queue {
private:
  vector<T> m_heap;

  unsigned int get_parent(int i) {
    int res = (i-1)/2;
    if (res > 0) {
      return res;
    } else {
      return 0;
    }
  }

  inline unsigned int get_left(int i) { return (2*i + 1); }
  inline unsigned int get_right(int i) { return (2*i + 2); }

  void swap(int i1, int i2) {
    T temp = this->m_heap[i1];
    this->m_heap[i1] = this->m_heap[i2];
    this->m_heap[i2] = temp;
  }

  int check_recurse(int i, const vector<T> &heap) {
    unsigned int left = this->get_left(i);
    unsigned int right = this->get_right(i);
    unsigned int size = heap.size();
    if (left < size && right < size) {
      if (heap[left] > heap[i] && heap[right] > heap[i]) {
        return check_recurse(left, heap) & check_recurse(right, heap);
      } else {
        return 0;
      }
    } else if (left < size) {
      if (heap[left] > heap[i]) {
        return check_recurse(left, heap);
      } else {
        return 0;
      }
    } else if (right < size) {
      if (heap[right] > heap[i]) {
        return check_recurse(right, heap);
      } else {
        return 0;
      }
    } else {
      return 1;
    }
  }

  void percolate_down(int i) {
    unsigned int left = this->get_left(i);
    unsigned int right = this->get_right(i);
    unsigned int size = this->m_heap.size();
    if (left < size && right < size) {
      if (m_heap[i] >= m_heap[left] && m_heap[i] >= m_heap[right]) {
        if (m_heap[left] > m_heap[right]) {
          swap(i, right);
          percolate_down(right);
        } else {
          swap(i, left);
          percolate_down(left);
        }
      }
    }
    if (left < size && m_heap[i] >= m_heap[left]) {
      swap(i, left);
      percolate_down(left);
    } else if (right < size && m_heap[i] >= m_heap[right]) {
      swap(i, right);
      percolate_down(right);
    }
  }

  void percolate_up(int current) {
    int parent = this->get_parent(current);
    if (parent >= 0 && this->m_heap[current] < this->m_heap[parent]) {
      swap(current, parent);
      percolate_up(parent);
    }
  }

public:
  priority_queue() {}

  priority_queue( vector<T> const& values )
  {
    for (unsigned int i = 0; i < values.size(); i++) {
      this->push(values[i]);
    }
  }

  const T& top() const 
  {
    assert( !m_heap.empty() );
    return m_heap[0]; 
  }

  void push( const T& entry )
  {
    // Push the new leaf onto the end
    this->m_heap.push_back(entry);
    int current = this->m_heap.size() - 1;
    percolate_up(current);
  }

  void pop() 
  {
    assert( !m_heap.empty() );
    swap(0, this->m_heap.size() - 1);
    this->m_heap.pop_back();
    percolate_down(0);
  }

  int size() { return m_heap.size(); }
  bool empty() { return m_heap.empty(); }


  //  The following three functions are used for debugging.

  //  Check to see that internally the heap property is realized.
  bool check_heap( )
  {
    return this->check_heap( this->m_heap );
  }

  //  Check an extrernal vector to see that the heap property is realized.
  bool check_heap( const vector<T>& heap )
  {
    return check_recurse(0, heap);
  }

  //  A utility to print the contents of the heap.  Use it for debugging.
  void print_heap( ostream & ostr )
  {
    for ( unsigned int i=0; i<m_heap.size(); ++i )
      ostr << i << ": " << m_heap[i] << endl;
  }
  
};


template <class T>
void heap_sort(std::vector<T> &vector)
{
  priority_queue<T> pq(vector);
  vector.clear();
  while (!pq.empty()) {
    vector.push_back(pq.top());
    pq.pop();
  }
}

#endif
