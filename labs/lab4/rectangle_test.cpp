#include <iostream>
#include "point2d.h"
#include "rectangle.h"

int main(int argc, char *argv[]) {
  Point2D p1(3, 4);
  Point2D p2(1, 7);
  Point2D pt(5, 3);

  Rectangle r1(Point2D(1, 1), Point2D(5, 5));
  std::cout << "rectangle with coordinates: (1, 1) to (5, 5)" << std::endl;
  r1.add_point(p1);
  r1.add_point(p2);
  print_rectangle(r1);
  std::cout << "The top right corner is (" << r1.upper_right_corner().x() << ", " << r1.upper_right_corner().y() << ")" << std::endl;
  std::cout << "The bottom left corner is (" << r1.lower_left_corner().x() << ", " << r1.lower_left_corner().y() << ")" << std::endl;

  bool res1 = r1.is_point_within(p1);
  std::cout << "is (3, 4) inside the rectangle: " << res1 << std::endl;

  bool res2 = r1.is_point_within(p2);
  std::cout << "is (1, 7) inside the rectangle: " << res1 << std::endl;

  std::cout << std::endl;
  std::cout << "rectangle with coordinates: (4, 5) to (8, 9)" << std::endl;
  Rectangle r2(Point2D(4, 5), Point2D(8, 9));
  print_rectangle(r2);
  std::cout << std::endl;
  std::cout << "rectangle with coordinates: (2, 1) to (5, 6)" << std::endl;
  Rectangle r3(Point2D(2, 1), Point2D(5, 6));
  print_rectangle(r3);

  pt.set(2, 7);
}
