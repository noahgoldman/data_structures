#include <iostream>
#include "rectangle.h"

Rectangle::Rectangle(const Point2D &lower_left, const Point2D &upper_right )
  : m_lower_left(lower_left), m_upper_right(upper_right)
{ }

bool
Rectangle::is_point_within(const Point2D &p ) const
{
  return
    ( p.x() <= m_upper_right.x() && p.y() <= m_upper_right.y() ) ||
    ( p.x() >= m_lower_left.x() && p.y() >= m_lower_left.y() );
}

bool Rectangle::add_point(const Point2D &p) {
  if (!this->is_point_within(p)) return false;
  for (int i = 0; i < this->m_points_contained.size(); i++) {
    if (m_points_contained[i].x() == p.x() && m_points_contained[i].y() == p.y()) return false;
  }

  m_points_contained.push_back(p);
}


vector<Point2D> points_in_both( Rectangle const& r1, Rectangle const& r2 )
{
  //  The following access the vector of points in the two rectangles
  //  without copying these vectors.  Instead, r1_points and r2_points
  //  are references to the vectors of points, but since they are
  //  constants neither the vectors nor the points within them can be
  //  changed.
  const vector<Point2D> & r1_points = r1.points_contained();
  const vector<Point2D> & r2_points = r2.points_contained();


}


void print_rectangle( Rectangle const& r1 )
{
  // get all the points that are inside the rectangle
  const vector<Point2D> & r1_points = r1.points_contained();
  
  // print the rectangle coordinates
  std::cout << "Rectangle: " 
       << r1.lower_left_corner().x() << "," << r1.lower_left_corner().y() << "--"
       << r1.upper_right_corner().x() << "," << r1.lower_left_corner().y() << std::endl;
       
  // print points that are inside
  std::cout << "Points inside:" << std::endl;
  for( vector<Point2D>::size_type i = 0; i < r1_points.size(); ++i ) {
    std::cout << r1_points[i].x() << "," << r1_points[i].y() << std::endl;
  }
}


